/**
 * @author Dave Sizer
 * @since 8/17/2015
 * 
 * An entry point for the SicillianDefense
 * Text UI
 */
package mockui;

public class FullMockUIDriver {

	public static void main(String[] args) {
		FullMockUI.getInstance().mainInteractLoop();

	}

}
