package mockui;

import model.*;

import java.awt.*;
import java.util.Scanner;

/**
 * Created by Tom on 8/4/2015.
 * Something so I could see the board. Will get rid of it later.
 */
public class MockUI {
    Board board;
    public static void main(String[] args) throws Exception
    {
        Board b = Board.getInstance();
        b.setupBoard();
        MockUI ui = new MockUI(b);
        ui.display();
        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.println("Enter a move(start x start y endx endy): ");
            System.out.print("> ");
            String input = sc.nextLine();
            if (input.equals("q"))
                return;

            String[] inList = input.split(" ");
            if(4 != inList.length) {
                System.out.println("wut");
                continue;
            }

            int sx, sy, dx, dy;
            try {
                sx = Integer.parseInt(inList[0]);
                sy = Integer.parseInt(inList[1]);
                dx = Integer.parseInt(inList[2]);
                dy = Integer.parseInt(inList[3]);
            } catch(Exception ex)
            {
                System.out.println("wut.");
                continue;
            }

            try {
                if (!b.movePiece(new Point(sx, sy), new Point(dx, dy)))
                    System.out.println("Failed to move.");
            } catch(Exception ex)
            {
                System.out.println("Piece not found at (" + sx +", " + sy + ").");
                continue;
            }

            ui.display();
        }
    }

    public MockUI(Board board)
    {
        this.board = board;
    }

    public void display()
    {
        for(int i=0; i<8; i++) {
            System.out.print("| ");
            for(int j=0; j<8; j++) {
                Piece p = board.getPiece(j, i);
                if(null == p)
                    System.out.print(" ");
                else {
                    if(p instanceof Pawn)
                        System.out.print("P");
                    else if(p instanceof Bishop)
                        System.out.print("B");
                    else if(p instanceof Rook)
                        System.out.print("R");
                    else if(p instanceof Knight)
                        System.out.print("N");
                    else if(p instanceof Queen)
                        System.out.print("Q");
                    else if(p instanceof King)
                        System.out.print("K");
                    else
                        System.err.print("ERROR");
                }
                System.out.print(" | ");
            }

            System.out.println();
            System.out.println("---------------------------------");
        }
        System.out.println();
        if(board.inCheck(model.Color.BLACK))
            System.out.println("Black is in check!");
        if(board.inCheck(model.Color.WHITE))
            System.out.println("White is in check!");
        if(board.inCheckmate(model.Color.BLACK))
                System.out.println("!!!!!Black is in checkmate!!!!!");
        if(board.inCheckmate(model.Color.WHITE))
            System.out.println("!!!!!White is in checkmate!!!!!");
    }

}
