/**
 * @author Dave Sizer
 * @since 8/17/2015
 * A text UI to help with testing
 * 
 * Uses code from MockUI written by Tom Amon
 */
package mockui;

import java.awt.*;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Scanner;

import model.*;
import view.panel.ActivePanel;
import controller.*;

public class FullMockUI {
	private static FullMockUI instance = null;
    private Board board;
    private Controller controller;
    private ActivePanel activePanel;
    private Scanner sc;
    private boolean boardLocked;
    private UncaughtExceptionHandler exHandler; // To handle exceptions thrown by other threads

    public static FullMockUI getInstance()
    {
    	if (null == instance)
    		instance = new FullMockUI();
    	return instance;
    }
    public FullMockUI()
    {
    	// Usually we would get the board instance with the correct color after
    	// determining who goes first
        board = Board.getInstance();
        board.setupBoard();
  
        sc = new Scanner(System.in);
        boardLocked = true;  // Should be true
        activePanel = ActivePanel.LOGIN; // This should actually default to login
        
        controller = Controller.getInstance();
    }
    
    public void setActivePanel (ActivePanel panel)
    {
    	activePanel = panel;
    }
    
    public void lock()
    {
    	boardLocked = true;
    }
    
    public void unlock()
    {
    	boardLocked = false;
    }
    
    public boolean isLocked()
    {
    	return boardLocked;
    }
    
    private void promptForMove()
    {
    	boolean done = false;
    	while(!done) {
            System.out.println("Enter a move(start x start y endx endy): ");
            System.out.print("> ");
            String input = sc.nextLine();
            if (input.equals("q"))
                return;

            String[] inList = input.split(" ");
            if(4 != inList.length) {
                System.out.println("wut");
                continue;
            }

            int sx, sy, dx, dy;
            try {
                sx = Integer.parseInt(inList[0]);
                sy = Integer.parseInt(inList[1]);
                dx = Integer.parseInt(inList[2]);
                dy = Integer.parseInt(inList[3]);
            } catch(Exception ex)
            {
                System.out.println("wut.");
                continue;
            }
            if (!controller.makeMove(new Point(sx, sy), new Point(dx, dy)))
            {
                System.out.println("Failed to move.");
                continue;
            }
            else
            	done = true;
        }
    }
    
    private void idlePrompt()
    {
    	System.out.println("The board is locked. Need help?");
    	displayHelp();
    	while (boardLocked)
    	{
    		try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
    
    public void displayHelp()
    {
    	System.out.println("See https://en.wikipedia.org/wiki/Chess");
    }
    
    
    public void mainInteractLoop()
    {
    	boolean done = false;
    	String panelString = "Panel: ";
    	
    	while (!done)
    	{
    		switch (activePanel)
    		{
    		case LOGIN:
    			System.out.println(panelString + "LOGIN");
    			System.out.println("Server or client?");
    			String serverinput = sc.nextLine();
		
    			
    			if (serverinput.toLowerCase().equals("server"))
    			{
    				activePanel = ActivePanel.HOST;
    			}
    			else if (serverinput.toLowerCase().equals("client"))
    			{
    				activePanel = ActivePanel.JOIN;
    			}
    			else
    				System.out.println("Invalid choice.");
    			
    			break;
    		
    		case HOST:
    			System.out.println(panelString + "HOST");
    			int port;
    			System.out.println("Server will run on IP:");
                System.out.println(controller.getServerIP());
    			System.out.println("What port will you be using?");
    			String portinput = "";
    			boolean portinputvalid = false;
    			while (!portinputvalid)
    			{
    				portinput = sc.nextLine();
			
    				portinputvalid = portinput.matches("\\d+");
			
    				if (!portinputvalid)
    					System.out.println("Please enter a number");
    			}
		
    			port = Integer.parseInt(portinput);
    			System.out.println("Waiting for opponent to connect...");
    			controller.initialize(NetworkMode.SERVER, null, port);
    			
    			break;
    		
    		case JOIN:
    			System.out.println(panelString + "JOIN");
    			String hostname;
    			System.out.println("What IP would you like to connect to?");
    			hostname = sc.nextLine();
    			portinputvalid = false;
    			portinput = "";
    			System.out.println("What port will you be using?");
    			while (!portinputvalid)
    			{
    				portinput = sc.nextLine();
			
    				portinputvalid = portinput.matches("\\d+");
			
    				if (!portinputvalid)
    					System.out.println("Please enter a number");
    			}
    			port = Integer.parseInt(portinput);
    			
    			System.out.println("Connecting to opponent...");
    			controller.initialize(NetworkMode.CLIENT, hostname, port);
    			
    			break;
    		case GAME:
    			System.out.println(panelString + "GAME");
    			if (boardLocked)
    				idlePrompt();
    			promptForMove();
    			break;
    			
    		default:
    			System.out.println("Somethin' terrible happenened, folks.");
    			break;
    		}
    	}
    }
    
    public void drawBoard()
    {
        System.out.println("---------------------------------");
        for(int i=7; i>=0; i--) {
            System.out.print("| ");
            for(int j=0; j<8; j++) {
                Piece p = board.getPiece(j, i);
                if(null == p)
                    System.out.print(" ");
                else {
                    if(p instanceof Pawn)
                        System.out.print("P");
                    else if(p instanceof Bishop)
                        System.out.print("B");
                    else if(p instanceof Rook)
                        System.out.print("R");
                    else if(p instanceof Knight)
                        System.out.print("N");
                    else if(p instanceof Queen)
                        System.out.print("Q");
                    else if(p instanceof King)
                        System.out.print("K");
                    else
                        System.err.print("ERROR");
                }
                System.out.print(" | ");
            }

            System.out.println();
            System.out.println("---------------------------------");
        }
        System.out.println();

    }

}
