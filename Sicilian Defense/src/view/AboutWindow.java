package view;

import javax.swing.*;
import java.awt.*;

/**
 * @nicholecasciato on 8/2/15.
 */
public class AboutWindow extends JFrame {
    private static AboutWindow aboutWindow;

    private int aboutWidth = 500;
    private int aboutHeight = 225;

    JLabel lblTitle = new JLabel("The Sicilian Defense", SwingConstants.CENTER);
    JLabel lblSubtitle = new JLabel("Never go in against a Sicilian when death is on the line!", SwingConstants.CENTER);
    JLabel lblVersion = new JLabel("Version 4.0.0.1", SwingConstants.CENTER);
    JLabel lblSpace = new JLabel();
    JLabel lblContributor1 = new JLabel("Thomas Amon", SwingConstants.CENTER);
    JLabel lblContributor2 = new JLabel("Nichole Casciato", SwingConstants.CENTER);
    JLabel lblContributor3 = new JLabel("Sarah Kushner", SwingConstants.CENTER);
    JLabel lblContributor4 = new JLabel("David Sizer", SwingConstants.CENTER);
    JLabel lblContributor5 = new JLabel("De Yu", SwingConstants.CENTER);

    private AboutWindow() {
        addComponents();

        this.setSize(new Dimension(aboutWidth, aboutHeight));
        this.setLocation(new Point((int) ((Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2) - (aboutWidth / 2)),
                (int) ((Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2) - (aboutHeight / 2))));
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.setLayout(new GridLayout(9, 0));
    }

    public static AboutWindow getInstance() {
        if (aboutWindow == null) {
            aboutWindow = new AboutWindow();
        }
        return aboutWindow;
    }

    private void addComponents() {
        lblTitle.setFont(new Font(new JLabel().getFont().getFontName(), Font.PLAIN, 18));
        lblSubtitle.setFont(new Font(new JLabel().getFont().getFontName(), Font.PLAIN, 16));

        this.add(lblTitle);
        this.add(lblSubtitle);
        this.add(lblVersion);
        this.add(lblSpace);
        this.add(lblContributor1);
        this.add(lblContributor2);
        this.add(lblContributor3);
        this.add(lblContributor4);
        this.add(lblContributor5);
    }
}