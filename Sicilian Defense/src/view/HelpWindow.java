package view;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * Created by nicholecasciato on 8/1/15.
 */
public class HelpWindow extends JFrame {
    private static HelpWindow helpWindow;

    private int helpWidth = (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 1.2);
    private int helpHeight = (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 1.2);

    JTabbedPane tabbedPane = new JTabbedPane();

    private String basicMoves = "Basic moves\n" +
            "- Pieces can move to any vacant space unless capturing an opponent's piece\n" +
            "- Pieces cannot jump other pieces (exception knights and castling)\n" +
            "- A piece is captured when attacking an opponent's piece (exception en passant)\n" +
            "- Kings can be put into check but they cannot be captured";

    private String pieceMoves = "Pawns\n" +
            "- Can move one or two spaces when moving from its starting position\n" +
            "- Subsequent movement is one square ahead\n" +
            "- Captures pieces diagonally only\n" +
            "- Involved in en passant and promotion (see \"Special Moves\" tab)\n\n" +
            "Rooks\n" +
            "- Can move any number of unblocked squares in a horizontal or vertical direction\n" +
            "- Involved in castling explained (see \"Special Moves\" tab)\n\n" +
            "Knights\n" +
            "- Moves two spaces in either a horizontal or vertical direction and then one square in a vertical or " +
            "horizontal direction (i.e. an \"L pattern\")\n" +
            "- Can jump pieces\n\n" +
            "Bishops\n" +
            "- Can move any number of unblocked squares in a horizontal, vertical, or diagonal direction\n\n" +
            "Queens\n" +
            "- Can move any number of unblocked squares in any direction\n\n" +
            "Kings\n" +
            "- Can move exactly one square in any direction\n" +
            "- Involved in castling explained (see \"Special Moves\" tab)";

    private String specialMove = "Castling\n" +
            "This is a move in which a player moves their King two squares toward a Rook and place that Rook on the" +
            " other side of the King\n\n" +
            "Permissible only in the following situations:\n" +
            "    - King and Rook involved must not have moved from their starting positions\n" +
            "    - No pieces are between the King and the Rook\n" +
            "    - The King is not in check\n" +
            "    - The King must not pass through a square that can be attacked by an opponent's piece\n\n" +
            "En passant\n" +
            "This is a move where an opponent's Pawn advances two spaces from its starting position and ends next to the" +
            " other player's Pawn on the same row. That player may capture that Pawn by moving behind it as if it had" +
            " been moved only one square. This is only legal in the turn after this board state.\n\n" +
            "Pawn promotion\n" +
            "This happens when a pawn advances to the opposite side of the board. The player promotes that piece to a" +
            " Rook, Knight, Bishop, or Queen. This does not depend on previously captured pieces so a player can have" +
            " more of one piece than was originally on the board.\n";

    private String check = "Check\n" +
            "- Occurs when a king is under attack by at least one enemy piece\n" +
            "- Must make a legal move to take the king out of check\n" +
            "- If there are no legal moves, this results in a checkmate (see below)\n\n" +
            "Checkmate\n" +
            "- Occurs when a player's king is in check and can make no legal move to escape\n" +
            "- Ends the game";

    private String otherEndGame = "Surrendering\n" +
            "- A player can surrender a game which results in an automatic loss on the surrender's part\n\n" +
            "Draws\n" +
            "- Stalemate - a player is not in check but has no legal moves\n" +
            "- No possibility of checkmate for either side, often due to insufficient pieces\n" +
            "    - King vs. King\n" +
            "    - King vs. King and Bishop\n" +
            "    - King vs. King and Knight\n" +
            "    - King and Bishop vs. King and Bishop (bishops are on squares of the same color\n" +
            "- Player agreement\n" +
            "- No pieces captured or pawns moved in the last 50 moves";

    private HelpWindow() {
        addComponents();

        this.setSize(new Dimension(helpWidth, helpHeight));
        this.setLocation(new Point((int) ((Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2) - (helpWidth / 2)),
                (int) ((Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 2) - (helpHeight / 2))));
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.setResizable(false);
    }

    public static HelpWindow getInstance() {
        if ( helpWindow == null ) {
            helpWindow = new HelpWindow();
        }
        return helpWindow;
    }

    private void addComponents() {
        JTextArea txtBasicMove = new JTextArea();
        JScrollPane basicMoveScroll = new JScrollPane(txtBasicMove);

        txtBasicMove.setText(basicMoves);
        txtBasicMove.setEditable(false);
        txtBasicMove.setLineWrap(true);
        txtBasicMove.setWrapStyleWord(true);

        basicMoveScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        basicMoveScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        tabbedPane.addTab("Basic Moves", basicMoveScroll);

        //------------------------------------------------------------------------------------------------------

        JTextArea txtPieceMove = new JTextArea();
        JScrollPane pieceMoveScroll = new JScrollPane(txtPieceMove);

        txtPieceMove.setText(pieceMoves);
        txtPieceMove.setEditable(false);
        txtPieceMove.setLineWrap(true);
        txtPieceMove.setWrapStyleWord(true);

        pieceMoveScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        pieceMoveScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        tabbedPane.addTab("Piece Moves", pieceMoveScroll);

        this.add(tabbedPane);

        //------------------------------------------------------------------------------------------------------

        JTextArea txtSpecialMove = new JTextArea();
        JScrollPane specialMoveScroll = new JScrollPane(txtSpecialMove);

        txtSpecialMove.setText(specialMove);
        txtSpecialMove.setEditable(false);
        txtSpecialMove.setLineWrap(true);
        txtSpecialMove.setWrapStyleWord(true);

        specialMoveScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        specialMoveScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        tabbedPane.addTab("Special Moves", specialMoveScroll);

        this.add(tabbedPane);

        //------------------------------------------------------------------------------------------------------

        JTextArea txtCheck = new JTextArea();
        JScrollPane checkScroll = new JScrollPane(txtCheck);

        txtCheck.setText(check);
        txtCheck.setEditable(false);
        txtCheck.setLineWrap(true);
        txtCheck.setWrapStyleWord(true);

        checkScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        checkScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        tabbedPane.addTab("Check/Checkmate", checkScroll);

        //------------------------------------------------------------------------------------------------------

        JTextArea txtOtherEndGame = new JTextArea();
        JScrollPane otherEndGameScroll = new JScrollPane(txtOtherEndGame);

        txtOtherEndGame.setText(otherEndGame);
        txtOtherEndGame.setEditable(false);
        txtOtherEndGame.setLineWrap(true);
        txtOtherEndGame.setWrapStyleWord(true);

        otherEndGameScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        otherEndGameScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        tabbedPane.addTab("Other End Games", otherEndGameScroll);

        this.add(tabbedPane);
    }
}