package view;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.*;


/**
 * Created by nicholecasciato on 8/23/15.
 */
public class HistoryWindow extends JFrame {
    private static HistoryWindow historyWindow;

    private int historyWidth = (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 4);
    private int historyHeight = (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() -
            (Toolkit.getDefaultToolkit().getScreenSize().getHeight()  / 25));

    private JTextArea txtHistory = new JTextArea();
    private JScrollPane historyScroll = new JScrollPane(txtHistory);

    private HistoryWindow() {
        txtHistory.setLineWrap(true);
        txtHistory.setWrapStyleWord(true);
        txtHistory.setEditable(false);

        historyScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.add(historyScroll);

        this.setTitle("Game History");
        this.setSize(new Dimension(historyWidth, historyHeight));
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.setVisible(true);
    }

    public static HistoryWindow getInstance() {
        if ( historyWindow == null ) {
            historyWindow = new HistoryWindow();
        }
        return historyWindow;
    }

    public void updateHistory(String history) {
        txtHistory.setText(history);
    }

    public void setHistoryVisible(boolean visible) {
        this.setVisible(visible);
    }
}