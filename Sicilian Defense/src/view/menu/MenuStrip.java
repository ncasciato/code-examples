package view.menu;

import javax.swing.JMenuBar;

/**
 * Created by nicholecasciato on 8/1/15.
 */
public class MenuStrip extends JMenuBar {
    private FileMenu fileMenu;
    private HelpMenu helpMenu;

    public MenuStrip() {
        fileMenu = new FileMenu();
        this.add(fileMenu);

        helpMenu = new HelpMenu();
        this.add(helpMenu);
    }

    public void setEnableSurrender(boolean enabled) {
        fileMenu.setEnableSurrender(enabled);
    }

//    public void setEnableDraw(boolean enabled) {
//        fileMenu.setEnableDraw(enabled);
//    }

    public void setEnableHistory(boolean enabled) {
        fileMenu.setEnableHistory(enabled);
    }
}