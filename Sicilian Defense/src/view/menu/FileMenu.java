package view.menu;

import controller.Controller;
import view.GameWindow;
import view.HistoryWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Created by nicholecasciato on 8/1/15.
 */
public class FileMenu extends JMenu implements ActionListener {
    private JMenuItem mnuSurrender;
//    private JMenuItem mnuDraw;
    private JMenuItem mnuHistory;
    private JMenuItem mnuExit;

    public FileMenu() {
        this.setText("File");

        surrenderMenu();
//        drawMenu();
        historyMenu();
        exitMenu();
    }

    private void surrenderMenu() {
        mnuSurrender = new JMenuItem("Surrender", KeyEvent.VK_S);
        mnuSurrender.setEnabled(false);
        mnuSurrender.addActionListener(this);
        this.add(mnuSurrender);
    }

//    private void drawMenu() {
//        mnuDraw = new JMenuItem("Draw", KeyEvent.VK_D);
//        mnuDraw.setEnabled(false);
//        mnuDraw.addActionListener(this);
//        this.add(mnuDraw);
//    }

    private void historyMenu() {
        mnuHistory= new JMenuItem("History", KeyEvent.VK_H);
        mnuHistory.setEnabled(false);
        mnuHistory.addActionListener(this);
        this.add(mnuHistory);
    }

    private void exitMenu () {
        mnuExit = new JMenuItem("Exit", KeyEvent.VK_X);
        mnuExit.addActionListener(this);
        this.add(mnuExit);
    }

    public void setEnableSurrender(boolean enabled) {
        mnuSurrender.setEnabled(enabled);
    }

//    public void setEnableDraw(boolean enabled) {
//        mnuDraw.setEnabled(enabled);
//    }

    public void setEnableHistory(boolean enabled) {
        mnuHistory.setEnabled(enabled);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(mnuSurrender)) {
            Controller.getInstance().surrender();
//        } else if (e.getSource().equals(mnuDraw)) {
//            Controller.getInstance().draw();
        } else if (e.getSource().equals(mnuHistory)) {
            HistoryWindow.getInstance().setHistoryVisible(true);
        } else if (e.getSource().equals(mnuExit)) {
            GameWindow.getInstance().exit();
        } else {
            JOptionPane.showMessageDialog(new JFrame(), "Something went catastrophically wrong!");
        }
    }
}