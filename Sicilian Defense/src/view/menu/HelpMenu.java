package view.menu;

import view.AboutWindow;
import view.HelpWindow;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

/**
 * Created by nicholecasciato on 8/1/15.
 */
public class HelpMenu extends JMenu implements ActionListener {
    private JMenuItem mnuAbout;
    private JMenuItem mnuHelp;

    public HelpMenu() {
        this.setText("Help");

        aboutMenu();
        helpMenu();
    }

    private void aboutMenu() {
        mnuAbout = new JMenuItem("About", KeyEvent.VK_A);
        this.add(mnuAbout);
        mnuAbout.addActionListener(this);
    }

    private void helpMenu() {
        mnuHelp = new JMenuItem("Help", KeyEvent.VK_H);
        this.add(mnuHelp);
        mnuHelp.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == mnuAbout) {
            AboutWindow.getInstance().setVisible(true);
        } else if (e.getSource() == mnuHelp) {
            HelpWindow.getInstance().setVisible(true);
        } else {
            JOptionPane.showMessageDialog(new JFrame(), "Something went catastrophically wrong!");
        }
    }
}