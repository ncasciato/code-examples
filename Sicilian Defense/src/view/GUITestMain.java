package view;

import view.panel.ActivePanel;
import view.panel.EndPanel;
import view.panel.HostPanel;

/**
 * @author Nichole Casciato
 * @since 8/1/2015
 */
public class GUITestMain {
    public static void main(String[ ] args) {
        GameWindow.getInstance().setVisible(true);
//        GameWindow.getInstance().changePanel(ActivePanel.GAME);

//        Host Panel manipulation example
//        GameWindow.getInstance().changePanel(ActivePanel.HOST);
//        ((HostPanel) GameWindow.getInstance().getActivePanel()).setIPAddress("127.0.0.0");
    }
}