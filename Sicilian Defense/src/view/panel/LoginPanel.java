package view.panel;

import controller.Controller;
import controller.NetworkMode;
import view.GameWindow;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.BoxLayout;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by nicholecasciato on 8/1/15.
 */
public class LoginPanel extends JPanel implements ActionListener {
    private int windowWidth;
    private int windowHeight;

    private JLabel lblTitle = new JLabel("The Sicilian Defense");
    private JLabel lblSubtitle = new JLabel("Never go in against a Sicilian when death is on the line!");

    private JButton btnHost = new JButton("Host");
    private JButton btnJoin = new JButton("Join");

    public LoginPanel(int windowWidth, int windowHeight) {
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;

        addComponents();
        this.setVisible(true);
    }

    private void addComponents() {
        // Empty panel for spacing purposes
        JPanel emptyPanel = new JPanel();
        emptyPanel.setMaximumSize(new Dimension(windowWidth, windowHeight / 4));

        // Label tweaks and panel
        lblTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblTitle.setFont(new Font(new JLabel().getFont().getFontName(), Font.BOLD, 24));

        lblSubtitle.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblSubtitle.setFont(new Font(new JLabel().getFont().getFontName(), Font.ITALIC, 20));

        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
        labelPanel.setMaximumSize(new Dimension(windowWidth, windowHeight / 4));
        labelPanel.add(lblTitle);
        labelPanel.add(lblSubtitle);

        // Button tweaks and panel
        btnHost.setFont(new Font(new JButton().getFont().getFontName(), Font.PLAIN, 18));
        btnHost.addActionListener(this);

        btnJoin.setFont(new Font(new JButton().getFont().getFontName(), Font.PLAIN, 18));
        btnJoin.addActionListener(this);

        JPanel buttonPanel = new JPanel(new GridLayout(0, 2));
        buttonPanel.setMaximumSize(new Dimension(windowWidth / 2, windowHeight / 8));
        buttonPanel.add(btnHost);
        buttonPanel.add(btnJoin);

        this.add(emptyPanel);
        this.add(labelPanel);
        this.add(buttonPanel);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnHost)) {
            Controller.getInstance().initialize(NetworkMode.SERVER, null, 4242);
        } else if (e.getSource().equals(btnJoin)) {
            GameWindow.getInstance().changePanel(ActivePanel.JOIN);
        } else {
            JOptionPane.showMessageDialog(new JFrame(), "Something went catastrophically wrong!");
        }
    }
}