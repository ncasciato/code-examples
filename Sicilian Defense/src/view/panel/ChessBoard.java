package view.panel;

import controller.Controller;
import model.Board;
import model.Piece;
import model.PieceType;
import view.GameWindow;

import javax.swing.*;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * @author Nichole Casciato
 * @since 8/1/2015
 */
public class ChessBoard extends JPanel implements ActionListener {
    private ArrayList<ArrayList<JButton>> squares;

    private Point startPoint = null;
    private Point endPoint = null;

    public ChessBoard() {
        makeBoard();
        setPieces();

        this.setLayout(new GridLayout(8, 8, 0, 0));
        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (((i % 2 == 0) && (j % 2 == 0)) || ((i % 2 == 1) && (j % 2 == 1))) {
                    squares.get(i).get(j).setBackground(Color.WHITE);
                } else if (((i % 2 == 0) && (j % 2 == 1)) || ((i % 2 == 1) && (j % 2 == 0))) {
                    squares.get(i).get(j).setBackground(Color.GRAY);
                }
            }
        }

        if (Controller.getInstance().myTurn()) {
            String square = e.getActionCommand();
            int x = Integer.parseInt(square.substring(0, 1));
            int y = Integer.parseInt(square.substring(2));

            if (startPoint == null) {
                Piece piece = Board.getInstance().getPiece(x, y);
                if (piece != null && piece.getColor().equals(Board.getInstance().getBottomColor())) {
                    startPoint = new Point(x, y);
                    ((JButton) e.getSource()).setBackground(Color.CYAN);
                } else if (piece != null && !piece.getColor().equals(Board.getInstance().getBottomColor())) {
                    GameWindow.getInstance().showMessage("You cannot move your opponent's piece!",
                            "There is a cheater in our midst", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                endPoint = new Point(x, y);
                if (!Controller.getInstance().makeMove(startPoint, endPoint)) {
                    ((JButton) e.getSource()).setBackground(Color.RED);
                }
                startPoint = null;
                endPoint = null;
            }
        } else {
            GameWindow.getInstance().showMessage("Not your move!", "Be Patient", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void makeBoard() {
        squares = new ArrayList<>();

        for (int i = 0; i < 8; i++) {
            ArrayList<JButton> tmpList = new ArrayList<>();

            for (int j = 0; j < 8; j++) {
                JButton tmpButton = new JButton();
                tmpButton.addActionListener(this);
                tmpList.add(tmpButton);
                tmpButton.setActionCommand(j + "," + i);

                if (((i % 2 == 0) && (j % 2 == 0)) || ((i % 2 == 1) && (j % 2 == 1))) {
                    tmpButton.setBackground(Color.WHITE);
                } else if (((i % 2 == 0) && (j % 2 == 1)) || ((i % 2 == 1) && (j % 2 == 0))) {
                    tmpButton.setBackground(Color.GRAY);
                }
                tmpButton.setOpaque(true);
                tmpButton.setBorderPainted(false);
                this.add(tmpButton);
            }
            squares.add(tmpList);
        }
    }

    public void setPieces() {
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                Piece piece =  Board.getInstance().getPiece(row, col);
                squares.get(col).get(row).setIcon(null);

                if (piece != null) {
                    if (piece.getColor() == model.Color.WHITE) {
                        squares.get(col).get(row).setIcon(getWhitePiece(piece.getType()));
                    } else {
                        squares.get(col).get(row).setIcon(getBlackPiece(piece.getType()));
                    }
                }
            }
        }
    }

    protected ImageIcon getWhitePiece(PieceType piece) {
        switch (piece) {
            case PAWN:
                return new ImageIcon(getClass().getResource("/images/wpawn.png"));
            case ROOK:
                return new ImageIcon(getClass().getResource("/images/wrook.png"));
            case KNIGHT:
                return new ImageIcon(getClass().getResource("/images/wknight.png"));
            case BISHOP:
                return new ImageIcon(getClass().getResource("/images/wbishop.png"));
            case QUEEN:
                return new ImageIcon(getClass().getResource("/images/wqueen.png"));
            case KING:
                return new ImageIcon(getClass().getResource("/images/wking.png"));
            default:
                return null;
        }
    }

    protected ImageIcon getBlackPiece(PieceType piece) {
        switch (piece) {
            case PAWN:
                return new ImageIcon(getClass().getResource("/images/bpawn.png"));
            case ROOK:
                return new ImageIcon(getClass().getResource("/images/brook.png"));
            case KNIGHT:
                return new ImageIcon(getClass().getResource("/images/bknight.png"));
            case BISHOP:
                return new ImageIcon(getClass().getResource("/images/bbishop.png"));
            case QUEEN:
                return new ImageIcon(getClass().getResource("/images/bqueen.png"));
            case KING:
                return new ImageIcon(getClass().getResource("/images/bking.png"));
            default:
                return null;
        }
    }

    @Override
    public final Dimension getPreferredSize() {
        Dimension d = super.getPreferredSize();
        Dimension prefSize;
        Component c = getParent();
        if (c == null) {
            prefSize = new Dimension ((int) d.getWidth(), (int) d.getHeight());
        } else if (c != null && c.getWidth() > d.getWidth() && c.getHeight() > d.getHeight()) {
            prefSize = c.getSize();
        } else {
            prefSize = d;
        }
        int w = (int) prefSize.getWidth();
        int h = (int) prefSize.getHeight();
        // the smaller of the two sizes
        int s = (w > h ? h : w);
        return new Dimension(s, s);
    }
}