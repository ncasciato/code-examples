package view.panel;

import controller.Controller;
import view.GameWindow;

import javax.swing.*;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by nicholecasciato on 8/1/15.
 */
public class EndPanel extends JPanel implements ActionListener{
    private int windowWidth;
    private int windowHeight;

    private JLabel lblMessage = new JLabel();
    private JButton btnSave = new JButton("Save Game History");
    private JButton btnPlayAgain = new JButton("Play Again");
    private JButton btnQuit = new JButton("Quit");

    public EndPanel(int windowWidth, int windowHeight) {
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;

        addComponents();
        this.setSize(new Dimension(windowWidth, windowHeight));
        this.setVisible(true);
    }

    private void addComponents() {
        // Empty panel for spacing purposes
        JPanel emptyPanel = new JPanel();
        emptyPanel.setMaximumSize(new Dimension(windowWidth, windowHeight / 4));

        // Label tweaks and panel
        lblMessage.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblMessage.setFont(new Font(new JLabel().getFont().getFontName(), Font.PLAIN, 50));

        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
        labelPanel.setMaximumSize(new Dimension(windowWidth, windowHeight / 4));
        labelPanel.add(lblMessage);

        // Button tweaks and panel
        btnSave.setFont(new Font(new JButton().getFont().getFontName(), Font.PLAIN, 18));
        btnSave.addActionListener(this);

        btnPlayAgain.setFont(new Font(new JButton().getFont().getFontName(), Font.PLAIN, 18));
        btnPlayAgain.addActionListener(this);

        btnQuit.setFont(new Font(new JButton().getFont().getFontName(), Font.PLAIN, 18));
        btnQuit.addActionListener(this);

        JPanel buttonPanel = new JPanel(new GridLayout(3, 0));
        buttonPanel.setMaximumSize(new Dimension(windowWidth / 3, windowHeight / 4));
        buttonPanel.add(btnSave);
        buttonPanel.add(btnPlayAgain);
        buttonPanel.add(btnQuit);

        this.add(emptyPanel);
        this.add(labelPanel);
        this.add(buttonPanel);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    public void setWinner() {
        lblMessage.setText("You win :)");
    }

    public void setLoser() {
        lblMessage.setText("You lose :(");
    }

    public void setDraw() {
        lblMessage.setText("It's a draw :|");
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnSave)) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

            int returnVal = fileChooser.showSaveDialog(this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                Controller.getInstance().saveFile(file);
            }
        } else if (e.getSource().equals(btnPlayAgain)) {
            Controller.getInstance().restart();
        } else if (e.getSource().equals(btnQuit)) {
            System.exit(0);
        } else {
            JOptionPane.showMessageDialog(new JFrame(), "Something went catastrophically wrong!");
        }
    }
}