package view.panel;

/**
 * Created by nicholecasciato on 8/1/15.
 */
public enum ActivePanel {
    LOGIN,
    HOST,
    JOIN,
    GAME,
    END;

    public static String toString(ActivePanel activePanel) {
        switch (activePanel) {
            case LOGIN:
                return "LOGIN";
            case HOST:
                return "HOST";
            case JOIN:
                return "JOIN";
            case GAME:
                return "GAME";
            case END:
                return "END";
            default:
                return "";
        }
    }
}