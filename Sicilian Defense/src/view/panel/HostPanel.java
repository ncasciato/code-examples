package view.panel;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;


/**
 * Created by nicholecasciato on 8/1/15.
 */
public class HostPanel extends JPanel {
    private int windowWidth;
    private int windowHeight;

    private JLabel lblTitle = new JLabel("The Sicilian Defense");
    private JLabel lblSubtitle = new JLabel("Never go in against a Sicilian when death is on the line!");
    private JLabel lblWaiting = new JLabel("Awaiting opponent...");
    private JLabel lblIPAddress = new JLabel("");

    public HostPanel(int windowWidth, int windowHeight) {
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;

        addComponents();
        this.setVisible(true);
    }

    private void addComponents() {
        // Empty panel for spacing purposes
        JPanel emptyPanel = new JPanel();
        emptyPanel.setMaximumSize(new Dimension(windowWidth, windowHeight / 4));

        // Label panel 1 and tweaks
        lblTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblTitle.setFont(new Font(new JLabel().getFont().getFontName(), Font.BOLD, 24));

        lblSubtitle.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblSubtitle.setFont(new Font(new JLabel().getFont().getFontName(), Font.ITALIC, 20));

        JPanel labelPanel1 = new JPanel();
        labelPanel1.setLayout(new BoxLayout(labelPanel1, BoxLayout.Y_AXIS));
        labelPanel1.setMaximumSize(new Dimension(windowWidth, windowHeight / 4));
        labelPanel1.add(lblTitle);
        labelPanel1.add(lblSubtitle);

        // Label panel 2 and tweaks
        lblWaiting.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblWaiting.setFont(new Font(new JLabel().getFont().getFontName(), Font.PLAIN, 18));

        lblIPAddress.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblIPAddress.setFont(new Font(new JLabel().getFont().getFontName(), Font.PLAIN, 18));

        JPanel labelPanel2 = new JPanel();
        labelPanel2.setLayout(new BoxLayout(labelPanel2, BoxLayout.Y_AXIS));
        labelPanel2.setMaximumSize(new Dimension(windowWidth, windowHeight / 2));
        labelPanel2.add(lblWaiting);
        labelPanel2.add(lblIPAddress);

        this.add(emptyPanel);
        this.add(labelPanel1);
        this.add(labelPanel2);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    public void setIPAddress(String address) {
        lblIPAddress.setText("Your IP address is " + address);
    }
}