package view.panel;

import model.Board;
import model.Color;
import model.Piece;
import model.PieceType;

import javax.swing.*;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

/**
 * @author Nichole Casciato
 * @since 8/1/2015
 */
public class GamePanel extends JPanel {
    private int windowWidth;
    private int windowHeight;

    private JLabel lblTurn = new JLabel();

    private JPanel myPieces = new JPanel();
    private JPanel opponentPieces = new JPanel();
    
    private JScrollPane myScroll = new JScrollPane(myPieces);
    private JScrollPane opponentScroll = new JScrollPane(opponentPieces);

    
    private ChessBoard chessBoard = new ChessBoard();

    public GamePanel(int windowWidth, int windowHeight) {
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;

        addComponents();
        this.setVisible(true);
    }

    private void addComponents() {
        lblTurn.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblTurn.setFont(new Font(new JLabel().getFont().getFontName(), Font.PLAIN, 30));

        JPanel labelPanel = new JPanel();
        labelPanel.setMaximumSize(new Dimension(windowWidth, windowHeight / 15));
        labelPanel.add(lblTurn);
        this.setLayout(new GridLayout(2, 1));

        JPanel boardPanel = new JPanel();
        boardPanel.setLayout(new BoxLayout(boardPanel, BoxLayout.X_AXIS));
        opponentScroll.setMaximumSize(new Dimension(windowWidth / 8, windowHeight));
        opponentScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        opponentPieces.setLayout(new BoxLayout(opponentPieces, BoxLayout.Y_AXIS));

        myScroll.setMaximumSize(new Dimension(windowWidth / 8, windowHeight));
        myScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        myPieces.setLayout(new BoxLayout(myPieces, BoxLayout.Y_AXIS));

        chessBoard.setMaximumSize(new Dimension(windowHeight, windowHeight));
        boardPanel.add(opponentScroll);
        boardPanel.add(chessBoard);
        boardPanel.add(myScroll);

        this.add(labelPanel);
        this.add(boardPanel);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    public void setMyTurn(boolean turn) {
        if (turn) {
            lblTurn.setText("Your Turn");
        } else {
            lblTurn.setText("Opponent's Turn");
        }
        chessBoard.setEnabled(turn);
    }

    public void repaintBoard() {
        JPanel whitePanel;
        JPanel blackPanel;

        if (Board.getInstance().getBottomColor() == Color.BLACK) {
            blackPanel = myPieces;
            whitePanel = opponentPieces;
        } else {
            whitePanel = myPieces;
            blackPanel = opponentPieces;
        }

        blackPanel.removeAll();
        for (Piece piece : Board.getInstance().getCapturedBlackPieces()) {
            JLabel tmpLabel = new JLabel(chessBoard.getBlackPiece(piece.getType()));
            tmpLabel.setHorizontalAlignment(SwingConstants.CENTER);
            blackPanel.add(tmpLabel);
        }

        whitePanel.removeAll();
        for (Piece piece : Board.getInstance().getCapturedWhitePieces()) {
            JLabel tmpLabel = new JLabel(chessBoard.getWhitePiece(piece.getType()));
            tmpLabel.setHorizontalAlignment(SwingConstants.CENTER);
            whitePanel.add(tmpLabel);
        }

        chessBoard.setPieces();
    }

    public PieceType promotePiece() {
        String pieces[] = {"Queen", "Bishop", "Knight", "Rook"};
        int result = JOptionPane.showOptionDialog(null, "Please pick a new piece:", "Piece Promotion",
                JOptionPane.PLAIN_MESSAGE, 0, null, pieces, pieces[3]);

        if (result == 3) {
            return PieceType.ROOK;
        } else if (result == 2) {
            return PieceType.KNIGHT;
        } else if (result == 1) {
            return PieceType.BISHOP;
        } else {
            return PieceType.QUEEN;
        }
    }
}