package view.panel;

import controller.Controller;
import controller.NetworkMode;
import sun.security.x509.IPAddressName;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import javax.swing.BoxLayout;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Nichole Casciato
 * @since 8/1/2015
 */
public class JoinPanel extends JPanel implements ActionListener {
    private int windowWidth;
    private int windowHeight;

    private JLabel lblTitle = new JLabel("The Sicilian Defense");
    private JLabel lblSubtitle = new JLabel("Never go in against a Sicilian when death is on the line!");

    private JLabel lblIPAddress = new JLabel("Enter a host address: ");
    private JTextField txtIPAddress = new JTextField();

    private JButton btnConnect = new JButton("Connect");

    public JoinPanel(int windowWidth, int windowHeight) {
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;

        addComponents();
        this.setVisible(true);
    }

    private void addComponents() {
        // Empty panel for spacing purposes
        JPanel emptyPanel = new JPanel();
        emptyPanel.setMaximumSize(new Dimension(windowWidth, windowHeight / 4));

        // Label panel 1 and tweaks
        lblTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblTitle.setFont(new Font(new JLabel().getFont().getFontName(), Font.BOLD, 24));

        lblSubtitle.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblSubtitle.setFont(new Font(new JLabel().getFont().getFontName(), Font.ITALIC, 20));

        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
        labelPanel.setMaximumSize(new Dimension(windowWidth, windowHeight / 4));
        labelPanel.add(lblTitle);
        labelPanel.add(lblSubtitle);

        // Label panel 2 and tweaks
        lblIPAddress.setAlignmentX(Component.CENTER_ALIGNMENT);
        lblIPAddress.setFont(new Font(new JLabel().getFont().getFontName(), Font.PLAIN, 18));

        txtIPAddress.setFont(new Font(new JTextField().getFont().getFontName(), Font.PLAIN, 18));

        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.X_AXIS));
        textPanel.setMaximumSize(new Dimension((int) (windowWidth / 1.5), 30));
        textPanel.add(lblIPAddress);
        textPanel.add(txtIPAddress);

        // Empty panel for spacing purposes
        JPanel emptyPanel2 = new JPanel();
        emptyPanel2.setMaximumSize(new Dimension(windowWidth, windowHeight / 8));

        // Button tweaks
        btnConnect.setFont(new Font(new JButton().getFont().getFontName(), Font.PLAIN, 16));
        btnConnect.addActionListener(this);

        JPanel buttonPanel = new JPanel(new GridLayout(1, 1));
        buttonPanel.setMaximumSize(new Dimension(windowWidth / 8, 40));
        buttonPanel.add(btnConnect);

        this.add(emptyPanel);
        this.add(labelPanel);
        this.add(textPanel);
        this.add(emptyPanel2);
        this.add(buttonPanel);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnConnect)) {
            Controller.getInstance().initialize(NetworkMode.CLIENT, txtIPAddress.getText(), 4242);
        } else {
            JOptionPane.showMessageDialog(new JFrame(), "Something went catastrophically wrong!");
        }
    }
}