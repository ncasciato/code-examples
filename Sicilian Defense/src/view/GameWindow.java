package view;

import view.menu.MenuStrip;

import view.panel.ActivePanel;
import view.panel.EndPanel;
import view.panel.GamePanel;
import view.panel.HostPanel;
import view.panel.JoinPanel;
import view.panel.LoginPanel;

import javax.swing.*;

import java.awt.*;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by nicholecasciato on 8/1/15.
 */
public class GameWindow extends JFrame {
    private static GameWindow gameWindow = null;

    private int windowWidth;
    private int windowHeight;

    private ActivePanel activePanel;
    private JPanel cards;
    private LoginPanel loginPanel;
    private HostPanel hostPanel;
    private JoinPanel joinPanel;
    private GamePanel gamePanel;
    private EndPanel endPanel;

    private GameWindow() {
        // Calculate Screen Size
        double height = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        windowWidth = (int) (height + (height * .25));
        windowHeight = (int) (height - (height / 25));

        loginPanel = new LoginPanel(windowWidth, windowHeight);
        hostPanel = new HostPanel(windowWidth, windowHeight);
        joinPanel = new JoinPanel(windowWidth, windowHeight);
        gamePanel = new GamePanel(windowWidth, windowHeight);
        endPanel = new EndPanel(windowWidth, windowHeight);

        activePanel = ActivePanel.LOGIN;
        cards = new JPanel(new CardLayout());
        cards.add(loginPanel, ActivePanel.toString(ActivePanel.LOGIN));
        cards.add(hostPanel, ActivePanel.toString(ActivePanel.HOST));
        cards.add(joinPanel, ActivePanel.toString(ActivePanel.JOIN));
        cards.add(gamePanel, ActivePanel.toString(ActivePanel.GAME));
        cards.add(endPanel, ActivePanel.toString(ActivePanel.END));

        // Window Settings
        this.setJMenuBar(new MenuStrip());
        this.setSize(new Dimension(windowWidth, windowHeight));
        this.setResizable(false);
        this.add(cards);

        this.setTitle("The Sicilian Defense");
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/wrook.png")));

        // Set Close Operations
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                exit();
            }
        });
    }

    public static GameWindow getInstance() {
        if (null == gameWindow) {
            gameWindow = new GameWindow();
        }
        return gameWindow;
    }

    public void changePanel(ActivePanel panel) {
        activePanel = panel;
        CardLayout card = (CardLayout) (cards.getLayout());
        card.show(cards, ActivePanel.toString(panel));
        if (panel == ActivePanel.GAME) {
            ((MenuStrip) this.getJMenuBar()).setEnableSurrender(true);
//            ((MenuStrip) this.getJMenuBar()).setEnableDraw(true);
            ((MenuStrip) this.getJMenuBar()).setEnableHistory(true);
            HistoryWindow.getInstance().setHistoryVisible(true);
        } else {
            ((MenuStrip) this.getJMenuBar()).setEnableSurrender(false);
//            ((MenuStrip) this.getJMenuBar()).setEnableDraw(false);
            ((MenuStrip) this.getJMenuBar()).setEnableHistory(false);
            HistoryWindow.getInstance().setVisible(false);
        }
    }

    public JPanel getActivePanel() {
        switch (activePanel) {
            case LOGIN:
                return loginPanel;
            case HOST:
                return hostPanel;
            case JOIN:
                return joinPanel;
            case GAME:
                return gamePanel;
            case END:
                return endPanel;
            default:
                return null;
        }
    }

    public void showMessage(String message, String title, int messageType) {
        JOptionPane.showMessageDialog(new JFrame(), message, title, messageType);
    }

    public int showConfirmDialog(String message, String title) {
        return JOptionPane.showConfirmDialog(null, message, title,  JOptionPane.YES_NO_OPTION);
    }

    public void exit() {
        int result = JOptionPane.showConfirmDialog(null,
                "Are you sure you want to exit the program?", "Exit Program Message Box",
                JOptionPane.YES_NO_OPTION);

        if (result == JOptionPane.YES_OPTION) {
            System.exit(1);
        }
    }
}