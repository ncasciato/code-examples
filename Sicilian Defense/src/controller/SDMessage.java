/**
 * @author Dave Sizer
 * @since 8/14/2015
 * 
 * The message class for network communications between
 * players of SicillianDefense
 */
package controller;

import model.PieceType;

import java.awt.Point;
import java.io.Serializable;

public class SDMessage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6864500422881376843L;
	private SDMessageType type;
	private boolean serverGoesFirst;
	private Point source, destination;
	private PieceType promotionPiece;
	
	/**
	 * @param type
	 */
	public SDMessage(SDMessageType type)
	{
		this.type = type;
	}
	
	/**
	 * @return
	 */
	public SDMessageType getType()
	{
		return type;
	}

	/**
	 * @return
	 */
	public Point getSource() {
		return source;
	}

	/**
	 * @param source
	 */
	public void setSource(Point source) {
		this.source = source;
	}

	/**
	 * @return
	 */
	public Point getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 */
	public void setDestination(Point destination) {
		this.destination = destination;
	}

	public PieceType getPromotionPiece() {
		return promotionPiece;
	}

	public void setPromotionPiece(PieceType promotionPiece) {
		this.promotionPiece = promotionPiece;
	}

	/**
	 * @return
	 */
	public boolean serverGoesFirst() {
		return serverGoesFirst;
	}

	/**
	 * @param serverGoesFirst
	 */
	public void setServerGoesFirst(boolean serverGoesFirst) {
		this.serverGoesFirst = serverGoesFirst;
	}
}
