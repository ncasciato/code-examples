package controller;

/**
 * @author chong
 * Message type
 *
 */
public enum SDMessageType {
	INIT, INIT_OK, MOVE, SERVER_WIN, CLIENT_WIN

}
