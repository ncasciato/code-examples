/**
 * @author Dave Sizer
 * @author Chong Yu
 * @since 8/8/2015
 * 
 * An interface for an application that can handle basic
 * networking tasks
 */
package controller;

import java.net.Socket;

public interface NetworkedApplication {
	public void networkActionSucceeded(Object obj);
	public void networkActionFailed(Exception e);
	public void connectionSucceeded(Socket socket);
	public void connectionFailed(Exception e) throws RuntimeException;

}
