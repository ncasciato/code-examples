/**
 * @author Dave Sizer
 * @since 8/8/2015
 * 
 * A thread worker for sending and receiving 
 * serialized objects over a socket
 */
package controller;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.Socket;
import java.net.SocketException;

public class NetWorker implements Runnable  {
	private NetworkedApplication app;
	private Serializable sendObject;
	private Socket socket;
	private boolean receiveLoop;
	private NetWorkerType type;
	private UncaughtExceptionHandler exHandler;

	/**
	 * @param type
	 * @param handler
	 * Basic constructor
	 * 
	 */
	public NetWorker(NetWorkerType type, UncaughtExceptionHandler handler) {
		app = null;
		sendObject = null;
		socket = null;
		receiveLoop = false;
		this.type = type;
		exHandler = handler;
	}
	
	/**
	 * Not being called anywhere, consider deleting it.
	 */
	public void killReceiveLoop()
	{
		receiveLoop = false;
	}

	/**
	 * @param app
	 * Basic setter
	 */
	public void setApplication(NetworkedApplication app) {
		this.app = app;
	}

	/**
	 * @param obj
	 * Basic setter
	 */
	public void setSendObject(Serializable obj) {
		this.sendObject = obj;
	}


	/**
	 * @param socket
	 * Basic setter
	 */
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
	/**
	 * 
	 */
	public void dispatch()
	{
		Thread t = new Thread(this);
		t.setUncaughtExceptionHandler(exHandler);
		t.start();
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		
		if (type == NetWorkerType.RECEIVE)
		{
			receiveLoop = true;
			// Run until killReceiveLoop() is called or the connection failed
			while(receiveLoop)
			{
				try {
					ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
					app.networkActionSucceeded(in.readObject());
				}
				catch (SocketException | EOFException e)
				{
					receiveLoop = false;
					app.connectionFailed(e);
				}
				catch (IOException | ClassNotFoundException e) {
					app.networkActionFailed(e);
				}
			}
		}
		else
		{
			try {
				ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
				out.writeObject(sendObject);
			}
			catch (SocketException e)
			{
				app.connectionFailed(e);
			}
			catch (IOException e) {
				app.networkActionFailed(e);
			}
		}
	}

}
