/**
 * @author Dave Sizer
 * @since 8/15/2015
 * 
 * A thread worker for establishing a connection between 
 * a server and a client
 */
package controller;

import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class ConnectionWorker implements Runnable {

	private ServerSocket serverSocket;
	private Socket clientSocket;
	private String hostName;
	private int port;
	private NetworkedApplication app;
	private NetworkMode networkMode;
	private UncaughtExceptionHandler exHandler;
	
	/**
	 * @param mode
	 * @param hostname
	 * @param port
	 * @param handler
	 * Basic constructor
	 */
	public ConnectionWorker(NetworkMode mode, String hostname, int port, UncaughtExceptionHandler handler)
	{
		this.hostName = hostname;
		this.port = port;
		this.app = null;
		this.networkMode = mode;
		exHandler = handler;
	}
	
	/**
	 * @param app
	 */
	public void setApplication(NetworkedApplication app)
	{
		this.app = app;
	}
	
	/**
	 * 
	 */
	public void dispatch()
	{
		Thread t = new Thread(this);
		t.setUncaughtExceptionHandler(exHandler);
		t.start();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 * Creates socket if server, 
	 * connects to socket if client
	 */
	@Override
	public void run() {
		if (null == app)
			return;
		
		if (networkMode == NetworkMode.CLIENT)
		{
			if (null == hostName)
			{
				app.connectionFailed(new Exception("ConnectionWorker dispatched in client mode without a hostname!"));
				return;
			}
			
			try 
			{
				clientSocket = new Socket(hostName, port);
				app.connectionSucceeded(clientSocket);
			} 
			catch (UnknownHostException e) 
			{
				app.connectionFailed(e);
			} 
			catch (IOException e) 
			{
				app.connectionFailed(e);
			}
		}
		else
		{
			try
			{
				serverSocket = new ServerSocket(port);
				clientSocket = serverSocket.accept();
				app.connectionSucceeded(clientSocket);
			} 
			catch (IOException e) 
			{
				app.connectionFailed(e);
			}
		}
		
	}

}
