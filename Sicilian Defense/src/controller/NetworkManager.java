/**
 * @author Dave Sizer
 * @author Chong Yu
 * @since 8/10/2015
 * A helper class for establishing and maintaining
 * TCP network connections between a server and client
 */
package controller;

import java.io.IOException;
import java.io.Serializable;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * @author chong
 *
 */
public class NetworkManager implements NetworkedApplication {
	
	private NetworkMode mode;
	private NetworkedApplication app;
	private Socket clientSocket;
	private int port;
	private boolean connected, connectionFailed;
	private UncaughtExceptionHandler exHandler;
	
	/**
	 * @param handler
	 * Basic constructor
	 */
	public NetworkManager(UncaughtExceptionHandler handler)
	{
		this.connected = false;
		this.connectionFailed = false;
		this.mode = null;
		exHandler = handler;
	}
	
	/**
	 * @param mode
	 * Basic setter
	 */
	public void setMode (NetworkMode mode)
	{
		this.mode = mode;
	}
	
	/**
	 * @param port
	 * Basic setter
	 */
	public void setPort(int port)
	{
		this.port = port;
	}
	
	
	/**
	 * @return
	 * check if socket is connected
	 */
	public boolean isConnected()
	{
		return (null != clientSocket)?clientSocket.isConnected():false;
	}/*
	public boolean connectionHasFailed()
	{
		return connectionFailed;
	}
	public String getConnectedHostname()
	{
		if (!connected)
			return "";
		return clientSocket.getInetAddress().getHostAddress();
	}*/
	
	
	/**
	 * @return serverAddress
	 */
	public ArrayList<String> getServerAddresses()
	{
			ArrayList<InetAddress> addresses = new ArrayList<InetAddress>();
			try {
				Enumeration<NetworkInterface> ifaceenum = NetworkInterface.getNetworkInterfaces();
				while (ifaceenum.hasMoreElements())
				{
					NetworkInterface ni = ifaceenum.nextElement();
					Enumeration<InetAddress> addrenum = ni.getInetAddresses();
					while (addrenum.hasMoreElements())
					{
						InetAddress thisaddr = addrenum.nextElement();
						addresses.add(thisaddr);
					}
				}
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			ArrayList<String> addressStrings = new ArrayList<String>();
			ArrayList<String> filteredStrings = new ArrayList<String>();
 			// Filter out loopback addresses
			for (InetAddress addr : addresses )
			{
				if (addr.isLoopbackAddress())
					continue;
				addressStrings.add(addr.getHostAddress());
			}
			
			// Filter out anything that isnt Ipv4
			for (String addr : addressStrings)
			{
				if (addr.matches("\\d+\\.\\d+\\.\\d+\\.\\d+"))
					filteredStrings.add(addr);
			}
			
			if (filteredStrings.isEmpty())
				filteredStrings.add("");
			return filteredStrings;
	}
	
	/**
	 * @param hostname
	 * @throws Exception
	 * init connection
	 */
	public void init(String hostname) throws Exception
	{
		if (null == mode || port == 0)
			throw new Exception("Tried to init NetworkManager without setting mode or port.");
			
		ConnectionWorker worker;
		worker = new ConnectionWorker(mode, hostname, port, exHandler);
		worker.setApplication(this);
		worker.dispatch();
		
	}
	
	/**
	 * @param app
	 * basic setter
	 */
	public void setAssociatedApplication(NetworkedApplication app)
	{
		this.app = app;
	}
	
	
	/**
	 * @param obj
	 * sending Serializable object 
	 */
	public void sendObject(Serializable obj)
	{
		// Create a NetWorker
		NetWorker worker = new NetWorker(NetWorkerType.SEND, exHandler);
		worker.setApplication(this);
		worker.setSocket(clientSocket);
		worker.setSendObject(obj);
		
		// Kick off the worker in a new thread
		worker.dispatch();
		
	}

	/* (non-Javadoc)
	 * @see controller.NetworkedApplication#networkActionSucceeded(java.lang.Object)
	 */
	@Override
	public void networkActionSucceeded(Object obj) {
		app.networkActionSucceeded(obj);
		
	}


	/* (non-Javadoc)
	 * @see controller.NetworkedApplication#networkActionFailed(java.lang.Exception)
	 */
	@Override
	public void networkActionFailed(Exception e) {
		app.networkActionFailed(e);
		
	}


	/* (non-Javadoc)
	 * @see controller.NetworkedApplication#connectionSucceeded(java.net.Socket)
	 */
	@Override
	public void connectionSucceeded(Socket socket) {
		clientSocket = socket;
		connected = true;
		connectionFailed = false;
		System.out.println("Successfully connected to " + clientSocket.getInetAddress());
		
		NetWorker receiveWorker = new NetWorker(NetWorkerType.RECEIVE, exHandler);
		receiveWorker.setApplication(this);
		receiveWorker.setSocket(clientSocket);
		receiveWorker.dispatch();
		
		app.connectionSucceeded(clientSocket);
	}


	/* (non-Javadoc)
	 * @see controller.NetworkedApplication#connectionFailed(java.lang.Exception)
	 */
	@Override
	public void connectionFailed(Exception e)  {
		connected = false;
		connectionFailed = true;
		app.connectionFailed(e);
	}

}
