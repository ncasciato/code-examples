/**
 * @author Dave Sizer
 * @author Chong Yu
 * @since 8/10/2015
 * The Controller class for SicillianDefense.
 * Handles all flow control of the application
 */
package controller;

import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.Socket;
import java.util.Random;

import javax.swing.JOptionPane;


import model.Board;
import model.Color;
import model.Piece;
import model.PieceType;
import tests.PieceTest;
import view.GameWindow;
import view.HistoryWindow;
import view.panel.*;
import view.panel.ActivePanel; 


public class Controller implements NetworkedApplication {

	private static Controller instance;
	private static boolean disableInstanceFailure = false; // For unit tests
	
	private Board board;
	private NetworkManager networkManager;
	private Random generator;
	boolean myTurn;
	private boolean clientInitConfirmed;
	private boolean serverInitReceived;
	private boolean moveReceived; // For unit tests
	private NetworkMode mode;
	private UncaughtExceptionHandler exHandler;
	

	/**
	 * @return instance
	 * create one if doesn't exists
	 */
	public static Controller getInstance() {
		
        if(null == instance)
        	instance = new Controller();
        
		return instance;
	}
	
	/**
	 * Constructor for controller, since it is an instance, it will be private
	 */
	private Controller() {

        exHandler = new UncaughtExceptionHandler() 
        {
        	public void uncaughtException(Thread th, Throwable ex) {
                //System.out.println(ex.getMessage());
                ex.printStackTrace(System.out);
        		GameWindow.getInstance().showMessage(ex.getMessage(), "PANIC", JOptionPane.ERROR_MESSAGE);
                //System.exit(0);
            }	
        };
		generator = new Random();
		clientInitConfirmed = false;
		networkManager = new NetworkManager(exHandler);
		moveReceived = false;
		serverInitReceived = false;
	}

	/**-- Code used for unit test --------**/
	public boolean moveReceived()
	{
		return moveReceived;
	}
	public boolean clientInitConfrirmed()
	{
		return clientInitConfirmed;
	}
	
	public boolean serverInitReceived()
	{
		return serverInitReceived;
	}

	
	public boolean isConnected()
	{
		return networkManager.isConnected();
	}


	
	public String getServerIP()
	{
		return networkManager.getServerAddresses().get(0);
	}
	
	/**-- End code used for unit test --------**/
	
	
	/**
	 * @return boolean
	 * Server runs a random generator to see who goes first
	 */
	public boolean serverGoesFirst()
	{
		int i = generator.nextInt(2);
		return (i == 0)?false:true;
	}


	/**
	 * @param mode
	 * @param hostname
	 * @param port
	 * Initialize  network info.
	 */
	public void initialize(NetworkMode mode, String hostname,
			int port) {
		System.out.println("DEBUG: Top of initialize");
		
		this.mode = mode;
		networkManager.setMode(mode);
		networkManager.setPort(port);
		networkManager.setAssociatedApplication(this);
		try {
			networkManager.init(hostname);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		if (mode == NetworkMode.SERVER)
		{
			GameWindow.getInstance().changePanel(ActivePanel.HOST);
			((HostPanel)(GameWindow.getInstance().getActivePanel())).setIPAddress(getServerIP());
		}
		else
		{
			System.out.println("DEBUG: Client bottom of initialize");
		}
	}
	
	/**
	 * @param point
	 * @return new point with correct location
	 * Because the board does not know the ui is flipped, we need to set the point to the 
	 * correct location
	 */
	public Point translatePoint(Point point)
	{
		return new Point(7-point.x, 7-point.y);
	}

	/**
	 * @param from
	 * @param to
	 * @return false if move is invalid
	 * @return true if it is valid move
	 */
	public boolean makeMove(Point from, Point to) {
		if (!board.movePiece(from, to))
			return false;

		SDMessage msg = new SDMessage(SDMessageType.MOVE);
		msg.setSource(from);
		msg.setDestination(to);
		msg.setPromotionPiece(null);

		Piece p = Board.getInstance().getPiece(to);
		if (p.getType() == PieceType.PAWN && Board.getInstance().validPromotion(p)) {
			PieceType result = ((GamePanel) GameWindow.getInstance().getActivePanel()).promotePiece();
			Board.getInstance().promote(p, result);
			msg.setPromotionPiece(result);
		}

		doTurn(msg);
		
		return true;

	}

	
	/**
	 * @param tosend
	 * finish turn
	 */
	public void doTurn(SDMessage tosend) {
		if (!myTurn)
		{
			//PANIC!!!!!!!!!
			panic("doTurn() called when it wasn't our turn");
			return;
		}
		((GamePanel) GameWindow.getInstance().getActivePanel()).repaintBoard();
		networkManager.sendObject(tosend);
		
		//TODO wins and draws
		
		if (!checkEndConditions(oppositeColor(board.getBottomColor())))
		{
			myTurn = !myTurn;
			((GamePanel) GameWindow.getInstance().getActivePanel()).setMyTurn(myTurn);
		}
		
	}
	
	public void surrender()
	{
		GameWindow.getInstance().exit();
	}
	
	private boolean checkCheck(Color color)
	{
		return board.inCheck(color);
	}
	
	private boolean checkMate(Color color)
	{
		return board.inCheckmate(color);
	}
	
	public boolean checkOptionalDraw()
	{
		if (board.fiftyMoveRule())
		{
			int res = GameWindow.getInstance().showConfirmDialog("Fifty moves have passed, call a draw?", "Optional Draw");
			switch (res)
			{
			case JOptionPane.YES_OPTION:
				return true;
			case JOptionPane.NO_OPTION:
				return false;
			default:
				panic("Controller got unhandled option from dialog");
			}
		}
		return false;
	}
	
	public boolean checkForceDraw(Color color)
	{
		return !board.isCheckmatePossible() || board.isInStalemate(color) || board.seventyFiveMoveRule();
	}
	

	/**
	 * @return myTurn 
	 * determines if it is your turn or not
	 */
	public boolean myTurn() {
		return myTurn;
	}
	/* (non-Javadoc)
	 * @see controller.NetworkedApplication#networkActionSucceeded(java.lang.Object)
	 * When networkAction is succeeded, 
	 * determine what action to take based on the received obj (init_ok, INIT, MOVE)
	 */
	@Override
	public void networkActionSucceeded(Object obj) {
		SDMessage received = (SDMessage)obj;
		switch (received.getType())
		{
		case INIT_OK:
			clientInitConfirmed = true;

			break;
		
		case INIT:
			System.out.println("DEBUG: Received init message");
			myTurn = !received.serverGoesFirst();
			
			SDMessage msg = new SDMessage(SDMessageType.INIT_OK);
			networkManager.sendObject(msg);
			if (myTurn)
			{
				board = Board.getInstance();
				board.setBottomColor(Color.WHITE);
				System.out.println("You are WHITE!");
				
			}
			else
			{
				board = Board.getInstance();
				board.setBottomColor(Color.BLACK);
				System.out.println("You are BLACK!");
			}
			GameWindow.getInstance().changePanel(ActivePanel.GAME);
			((GamePanel) GameWindow.getInstance().getActivePanel()).setMyTurn(myTurn);
			((GamePanel) GameWindow.getInstance().getActivePanel()).repaintBoard();
			serverInitReceived=true;
			break;
			
		case MOVE:
			if (myTurn)
			{// PANIC!!!!  GameWindow.exit()
				panic("Received MOVE message during our own turn");
				return;
			} 
			board.forceMovePiece(translatePoint(received.getSource()), translatePoint(received.getDestination()));
			board.promote(Board.getInstance().getPiece(translatePoint(received.getDestination())),
					received.getPromotionPiece());
			//TODO handle wins and ties
			if (!checkEndConditions(board.getBottomColor()))
			{
				myTurn = !myTurn;
				((GamePanel) GameWindow.getInstance().getActivePanel()).setMyTurn(myTurn);			
				((GamePanel) GameWindow.getInstance().getActivePanel()).repaintBoard();
			}
			moveReceived = true;
			break;
		default:
			panic("Unhandled message type");
			break;
		}
	}
	
	private Color oppositeColor(Color color)
	{
		return (color == Color.WHITE)?Color.BLACK:Color.WHITE;
	}
	
	private boolean checkEndConditions(Color color)
	{
		if (checkMate(color))
		{
			//TODO switch to end screen
			// if color is my color, lose, else win
			GameWindow.getInstance().changePanel(ActivePanel.END);
			if (color == board.getBottomColor())
			{
				// lose
				((EndPanel) GameWindow.getInstance().getActivePanel()).setLoser();
			}
			else
			{
				// win
				((EndPanel) GameWindow.getInstance().getActivePanel()).setWinner();
			}
			return true;
		}
		else if (checkCheck(color))
		{
			//TODO notify of check
			// if color is mycolor, i am in check, else they are
			if (color == board.getBottomColor())
			{
				GameWindow.getInstance().showMessage("You are in check.", "Check", JOptionPane.WARNING_MESSAGE);
			}
			else
			{
				GameWindow.getInstance().showMessage("Your opponent is in check.", "Opponent in Check", JOptionPane.WARNING_MESSAGE);
			}
			return false;
		}
		else if (checkForceDraw(color) || checkOptionalDraw())
		{
			//TODO switch to end/draw scree
			GameWindow.getInstance().changePanel(ActivePanel.END);
			((EndPanel) GameWindow.getInstance().getActivePanel()).setDraw();

			return true;
		}
		else return false;
	}
	
	/**
	 * @param message
	 * @throws RuntimeException
	 * Creates a panic box if something horrible has happened
	 */
	private void panic(String message) throws RuntimeException
	{
		throw new RuntimeException("PANIC: " + message);
	}

	@Override
	public void networkActionFailed(Exception e) {

	}

	/* (non-Javadoc)
	 * @see controller.NetworkedApplication#connectionSucceeded(java.net.Socket)
	 */
	@Override
	public void connectionSucceeded(Socket socket) {

		if(mode == NetworkMode.SERVER)
		{
			System.out.println("DEBUG: Server connection succeeded");
			myTurn = serverGoesFirst();
			
			if (myTurn)
			{
				board = Board.getInstance();
				board.setBottomColor(Color.WHITE);
				System.out.println("You are WHITE!");			
			}
			else
			{
				board = Board.getInstance();
				board.setBottomColor(Color.BLACK);
				System.out.println("You are BLACK!");
			}
			
			SDMessage initMsg = new SDMessage(SDMessageType.INIT);
			initMsg.setServerGoesFirst(myTurn);
			System.out.println("DEBUG: Server sending init message...");
			networkManager.sendObject(initMsg);
			while (!clientInitConfirmed)
			{
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			

			GameWindow.getInstance().changePanel(ActivePanel.GAME);
			((GamePanel) GameWindow.getInstance().getActivePanel()).setMyTurn(myTurn);
			((GamePanel) GameWindow.getInstance().getActivePanel()).repaintBoard();
		}
		

		
		// Client will switch to board which will be in locked state until we 
		// receive an INIT message
	}

	@Override
	public void connectionFailed(Exception e) {
		panic("Connection failed.");
	}

	public void restart() {
		myTurn = serverGoesFirst();

		if (myTurn)
		{
			board = Board.getInstance();
			board.setBottomColor(Color.WHITE);
		}
		else
		{
			board = Board.getInstance();
			board.setBottomColor(Color.BLACK);
		}

		SDMessage initMsg = new SDMessage(SDMessageType.INIT);
		initMsg.setServerGoesFirst(myTurn);
		System.out.println("DEBUG: Server sending init message...");
		networkManager.sendObject(initMsg);
		while (!clientInitConfirmed)
		{
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		GameWindow.getInstance().changePanel(ActivePanel.GAME);
		((GamePanel) GameWindow.getInstance().getActivePanel()).setMyTurn(myTurn);
		((GamePanel) GameWindow.getInstance().getActivePanel()).repaintBoard();
	}

	public void saveFile(File file) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			writer.write(Board.getInstance().getHistory());
			writer.close();
		} catch (IOException e) {
			GameWindow.getInstance().showMessage("File could not be written!", "Error!", JOptionPane.ERROR_MESSAGE);
		}
	}
}
