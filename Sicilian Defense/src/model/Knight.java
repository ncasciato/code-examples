package model;


import java.awt.Point;
import java.util.ArrayList;

/**
 * Created by Tom on 7/27/2015.
 * Edited by Sarah on 8/18/2015.
 */
public class Knight extends Piece {

    /**
     * @param color
     * @param point
     * basic constructor
     */
    public Knight(Color color, Point point)
    {
        super(color, point);
    }

    /* (non-Javadoc)
     * @see model.Piece#validateMove(java.awt.Point, model.Board)
     */
    public boolean validateMove(Point dest, Board board)
    {
        Piece victim = board.getPiece(dest);
        if(outsideBounds(dest))
            return false;
        else if(null != victim && victim.color == color)
            return false;

        int x_dis = dest.x - point.x;
        int y_dis = dest.y - point.y;

        if(Math.abs(x_dis) == 1 && Math.abs(y_dis) == 2)
            return true;
        else if(Math.abs(x_dis) == 2 && Math.abs(y_dis) == 1)
            return true;

        return false;
    }

    /* (non-Javadoc)
     * @see model.Piece#getAttackPath(model.Piece, model.Board)
     */
    public ArrayList<Point> getAttackPath(Piece victim, Board board)
    {
        ArrayList<Point> path = new ArrayList<>();
        int x_dis = victim.getPoint().x - point.x;
        int y_dis = victim.getPoint().y - point.y;

        if((Math.abs(x_dis) == 1 && Math.abs(y_dis) == 2) || (Math.abs(x_dis) == 2 && Math.abs(y_dis) == 1))
            path.add(this.getPoint());
        return path;
    }

    /* (non-Javadoc)
     * @see model.Piece#getType()
     */
    public PieceType getType()
    {
        return PieceType.KNIGHT;
    }
}
