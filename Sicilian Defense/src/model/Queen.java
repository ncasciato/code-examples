package model;

import java.awt.Point;
import java.util.ArrayList;

/**
 * Created by Tom on 7/27/2015.
 */
public class Queen extends Piece {

    /**
     * @param color
     * @param point
     */
    public Queen(Color color, Point point)
    {
        super(color, point);
    }

    /* (non-Javadoc)
     * @see model.Piece#validateMove(java.awt.Point, model.Board)
     */
    public boolean validateMove(Point dest, Board board)
    {
        // move like a rook
        Rook internalRook = new Rook(color, point);
        // or move like a bishop
        Bishop internalBishop = new Bishop(color, point);

        return internalRook.validateMove(dest, board) || internalBishop.validateMove(dest, board);

    }

    /* (non-Javadoc)
     * @see model.Piece#getAttackPath(model.Piece, model.Board)
     */
    public ArrayList<Point> getAttackPath(Piece victim, Board board)
    {
        // path like a rook
        Rook internalRook = new Rook(color, point);
        // or path like a bishop
        Bishop internalBishop = new Bishop(color, point);
        ArrayList<Point> rook_path = internalRook.getAttackPath(victim, board);
        if(rook_path.isEmpty())
            return internalBishop.getAttackPath(victim, board);
        else
            return rook_path;
    }

    /* (non-Javadoc)
     * @see model.Piece#getType()
     */
    public PieceType getType()
    {
        return PieceType.QUEEN;
    }
}
