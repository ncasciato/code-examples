package model;

import view.GameWindow;
import view.HistoryWindow;

import javax.swing.*;
import java.awt.Point;
import java.util.ArrayList;

/**
 * Created by Tom on 7/27/2015.
 * Edited by Sarah on 8/21/2015.
 */
public class Board
{
    private static Board instance;
    private Piece[][] board;
    private ArrayList<Piece> capturedWhitePieces;
    private ArrayList<Piece> capturedBlackPieces;
    private King whiteKing;
    private King blackKing;
    private Color bottomColor;
    private float moveCount = 0;
    private Pawn opponentEnPassantPawn;
    private Pawn myEnPassantPawn;
    private boolean showMessages = true;

    private String history = "";

    private Board()
    {
        bottomColor = Color.WHITE;
        clearBoard();
    }

    public Piece getPiece(Point point)
    {
        return getPiece(point.x, point.y);
    }

    public Piece getPiece(int x, int y)
    {
        if(outsideBounds(x, y))
            return null;

        return board[x][y];
    }

    public boolean movePiece(Point start, Point dest) {
        Piece piece = getPiece(start);
        return null != piece && movePiece(piece, dest);
    }

    public boolean movePiece(Piece piece, Point dest) {
        Piece victim = getPiece(dest);
        if(moveIsCastling(piece, dest))
            doCastling(piece, dest);
        else if(moveIsEnPassant(piece, dest))
            victim = doEnPassant(piece, dest);
        else if (!piece.validateMove(dest, this))
            return false;

        Pawn pawn = piece instanceof Pawn ? (Pawn)piece : null;
        boolean setEnPassantFlag = false;
        if(null != pawn && moveIsDoubleMove(piece, dest)) {
            myEnPassantPawn = pawn;
            pawn.setCanBeEnPassant(true);
            setEnPassantFlag = true;
        }
        boolean inCheckOriginally = inCheck(piece.getColor());

        Point originalPoint = piece.getPoint();
        move(piece, dest);

        //if a move results in being in check, then it's not legal. Undo the move
        if(inCheck(piece.getColor())) {
            if(inCheckOriginally)
                showMessage("You must remove yourself from check.", "Check");
            else
                showMessage("That move would put you in check.", "Check");
            undoLastMove(victim, piece, originalPoint);
            if(setEnPassantFlag) {
                myEnPassantPawn = null;
                pawn.setCanBeEnPassant(false);
            }
            return false;
        }

        King king = piece instanceof King ? (King)piece : null;
        Rook rook = piece instanceof Rook ? (Rook)piece : null;

        if(null != king)
            king.setCanCastle(false);
        else if(null != rook)
            rook.setCanCastle(false);
        else if(null != pawn)
            pawn.setFirstMove(false);

        if(null != opponentEnPassantPawn) {
            opponentEnPassantPawn.setCanBeEnPassant(false);
            opponentEnPassantPawn = null;
        }

        countMoves(piece, victim);
        recordHistory(originalPoint, dest);
        if(null != victim )
            recordCapture(victim, piece);

        return true;
    }

    public void forceMovePiece(Point start, Point dest)
    {
        Piece piece = getPiece(start);
        if (null == piece)
            return;
        forceMovePiece(piece, dest);
    }

    public void forceMovePiece(Piece piece, Point dest)
    {
        Point originalPoint = piece.getPoint();
        Piece victim = getPiece(dest);
        if(moveIsCastling(piece, dest))
            doCastling(piece, dest);
        else if(moveIsEnPassant(piece, dest))
            victim = doEnPassant(piece, dest);
        else if(moveIsDoubleMove(piece, dest)) {
            Pawn pawn = (Pawn) piece;
            opponentEnPassantPawn = pawn;
            pawn.setCanBeEnPassant(true);
        }
        if(null != myEnPassantPawn) {
            myEnPassantPawn.setCanBeEnPassant(false);
            myEnPassantPawn = null;
        }

        move(piece, dest);
        countMoves(piece, victim);
        recordHistory(originalPoint, dest);
        if(null != victim )
            recordCapture(victim, piece);
    }

    void move(Piece piece, Point dest)
    {
        Piece victim = getPiece(dest);
        if (null != victim)
            addToGraveyard(victim);

        removePiece(piece.point);
        piece.setPoint(dest);
        placePiece(piece);
    }

    private void countMoves(Piece piece, Piece victim)
    {
        if (null != victim || piece instanceof Pawn)
            moveCount = 0;
        else
            moveCount += .5;
    }

    public boolean isCheckmatePossible()
    {
        Bishop whiteBishop = null;
        Bishop blackBishop = null;
        int knightCount = 0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (getPiece(i, j) instanceof Queen)
                    return true;
                else if (getPiece(i, j) instanceof Pawn)
                    return true;
                else if (getPiece(i, j) instanceof Rook)
                    return true;
                else if (getPiece(i, j) instanceof Bishop) {
                    Piece b = getPiece(i, j);
                    if (b.getColor() == Color.WHITE) {
                        if (null == whiteBishop)
                            whiteBishop = (Bishop) b;
                        else
                            return true;
                    }
                    else {
                        if (null == blackBishop)
                            blackBishop = (Bishop) b;
                        else
                            return true;
                    }
                }
                else if (getPiece(i, j) instanceof Knight)
                    knightCount++;
            }
        }

        if (knightCount > 1)
            return true;

        if (null != whiteBishop && null != blackBishop) {
            Point bPoint = blackBishop.getPoint();
            Point wPoint = whiteBishop.getPoint();
            if (((bPoint.x + bPoint.y) % 2 == 0) && ((wPoint.x + wPoint.y) % 2 == 0))
                return false;
            else if (((bPoint.x + bPoint.y) % 2 != 0) && ((wPoint.x + wPoint.y) % 2 != 0))
                return false;
            else
                return true;
        }

        return false;
    }

    public boolean isInStalemate(Color color)
    {
        ArrayList<Piece> team = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (null != getPiece(i, j))
                    if (getPiece(i, j).getColor() == color)
                        team.add(getPiece(i, j));
            }
        }

        for (Piece member : team) {
            Point originalPoint = member.getPoint();
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    Point p = new Point(i, j);
                    if (member.getPoint() != p)
                        if (member.validateMove(p, this)) {
                                Piece victim = getPiece(i, j);
                                move(member, p);
                                if(!inCheck(member.getColor())) {
                                    undoLastMove(victim, member, originalPoint);
                                    System.out.print(i + ", " + j);
                                    return false;
                                }
                                undoLastMove(victim, member, originalPoint);
                        }
                }
            }
        }

        return true;
    }

    public boolean fiftyMoveRule()
    {
        return moveCount >= 50;
    }

    public boolean seventyFiveMoveRule()
    {
        return moveCount >= 75;
    }

    public boolean inCheck(Color color)
    {
        King king = getKing(color);
        ArrayList<Piece> threats = getThreats(king);
        if (!threats.isEmpty()) {
            recordCheck(color);
        }
        return !threats.isEmpty();
    }

    public boolean inCheckmate(Color color)
    {
        King king = getKing(color);
        ArrayList<Piece> threats = getThreats(king);
        if(threats.isEmpty())
            return false;  // king isn't even in check

        int x = king.getPoint().x;
        int y = king.getPoint().y;
        Point[] pointsToCheck = new Point[8];
        pointsToCheck[0] = new Point(x+1,y);
        pointsToCheck[1] = new Point(x-1,y);
        pointsToCheck[2] = new Point(x,y+1);
        pointsToCheck[3] = new Point(x,y-1);
        pointsToCheck[4] = new Point(x+1,y+1);
        pointsToCheck[5] = new Point(x+1,y-1);
        pointsToCheck[6] = new Point(x-1,y+1);
        pointsToCheck[7] = new Point(x-1,y-1);

        // if the king can move out of check, then no checkmate
        for(Point p : pointsToCheck) {
            Piece victim = getPiece(p);
            if(king.validateMove(p, this)) {
                boolean escapeRoute = false;
                move(king, p);
                if(!inCheck(king.getColor()))
                   escapeRoute = true;
                undoLastMove(victim, king, new Point(x,y));
                if(escapeRoute)
                    return false;
            }
        }

        // if there is more than 1 threat, you can't stop them all. Checkmate
        if(threats.size() > 1) {
            recordCheckmate(color);
            return true;
        }

        // if a piece can attack or block the only threat, then no checkmate
        Piece threat = threats.get(0);
        ArrayList<Point> path = threat.getAttackPath(king, this);
        for(Point point : path) {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    Piece piece = getPiece(i,j);
                    Piece victim = getPiece(point);
                    if (null != piece && piece.getColor() != threat.getColor() && piece.validateMove(point, this)) {
                        boolean escapeRoute = false;
                        move(piece, point);
                        if(!inCheck(king.getColor()))
                            escapeRoute = true;
                        undoLastMove(victim, piece, new Point(i, j));
                        if(escapeRoute)
                            return false;
                    }
                }
            }
        }

        return true;
    }

    public boolean validPromotion(Piece piece)
    {
        Pawn pawn = piece instanceof Pawn ? (Pawn)piece : null;
        return pawn != null && pawn.canBePromoted(this);
    }

    public void promote(Piece piece, PieceType replacementType)
    {
        if(!validPromotion(piece))
            return;

        Piece replacement;
        Color color = piece.getColor();
        Point point = piece.getPoint();
        switch(replacementType)
        {
            case ROOK:
                replacement = new Rook(color, point);
                break;
            case BISHOP:
                replacement = new Bishop(color, point);
                break;
            case QUEEN:
                replacement = new Queen(color, point);
                break;
            case KNIGHT:
                replacement = new Knight(color, point);
                break;
            default:
                return;
        }
        removePiece(piece.getPoint());
        placePiece(replacement);

        recordPromotion(replacement);
    }

    public void setBottomColor(Color color)
    {
        bottomColor = color;
        setupBoard();
    }

    public Color getBottomColor()
    {
        return bottomColor;
    }

    public ArrayList<Piece> getCapturedWhitePieces()
    {
        return capturedWhitePieces;
    }

    public ArrayList<Piece> getCapturedBlackPieces()
    {
        return capturedBlackPieces;
    }

    public static Board getInstance()
    {
        return getInstance(Color.WHITE);
    }

    public static Board getInstance(Color color)
    {
        if(null == instance)
        {
            instance = new Board();
            instance.setBottomColor(color);
        }
        return instance;
    }

    public void setupBoard()
    {
        Color topColor;
        int king_x, queen_x;
        if(bottomColor == Color.WHITE) {
            topColor = Color.BLACK;
            king_x = 4;
            queen_x = 3;
        }
        else {
            topColor = Color.WHITE;
            king_x = 3;
            queen_x = 4;
        }

        clearBoard();

        Rook rooklb = new Rook(bottomColor, new Point(0,7));
        Knight knightlb = new Knight(bottomColor, new Point(1,7));
        Bishop bishoplb = new Bishop(bottomColor, new Point(2,7));
        Queen queenb = new Queen(bottomColor, new Point(queen_x,7));
        King kingb = new King(bottomColor, new Point(king_x,7));
        Bishop bishoprb = new Bishop(bottomColor, new Point(5,7));
        Knight knightrb = new Knight(bottomColor, new Point(6,7));
        Rook rookrb = new Rook(bottomColor, new Point(7,7));

        Rook rooklw = new Rook(topColor, new Point(0,0));
        Knight knightlw = new Knight(topColor, new Point(1,0));
        Bishop bishoplw = new Bishop(topColor, new Point(2,0));
        Queen queenw = new Queen(topColor, new Point(queen_x,0));
        King kingw = new King(topColor, new Point(king_x, 0));
        Bishop bishoprw = new Bishop(topColor, new Point(5,0));
        Knight knightrw = new Knight(topColor, new Point(6,0));
        Rook rookrw = new Rook(topColor, new Point(7,0));

        placePiece(rooklb);
        placePiece(knightlb);
        placePiece(bishoplb);
        placePiece(kingb);
        placePiece(queenb);
        placePiece(bishoprb);
        placePiece(knightrb);
        placePiece(rookrb);
        for(int i=0; i<8; i++)
        {
            Pawn pawnb = new Pawn(bottomColor, new Point(i,6));
            placePiece(pawnb);
        }

        placePiece(rooklw);
        placePiece(knightlw);
        placePiece(bishoplw);
        placePiece(kingw);
        placePiece(queenw);
        placePiece(bishoprw);
        placePiece(knightrw);
        placePiece(rookrw);
        for(int i=0; i<8; i++)
        {
            Pawn pawnw = new Pawn(topColor, new Point(i,1));
            placePiece(pawnw);
        }
    }

    public void clearBoard()
    {
        moveCount = 0;
        capturedWhitePieces = new ArrayList<>();
        capturedBlackPieces = new ArrayList<>();

        board = new Piece[8][8];
        for(int i=0; i<8; i++) {
            for(int j=0; j<8; j++)
                board[i][j] = null;
        }

        whiteKing = null;
        blackKing = null;
    }

    public String getHistory() {
        return history;
    }

    public void setShowMessages(boolean doIt) {
        showMessages = doIt;
    }

//region Helpers

    private boolean moveIsEnPassant(Piece piece, Point dest)
    {
        int x_dis = dest.x - piece.point.x;
        int y_dis = dest.y - piece.point.y;

        Pawn pawn = piece instanceof Pawn ? (Pawn)piece : null;
        return null != pawn && pawn.validEnPassant(x_dis, y_dis, this);
    }

    private Piece doEnPassant(Piece piece, Point dest)
    {
        Piece victim;
        int x_dis = dest.x - piece.point.x;
        Point victim_point = new Point(piece.point.x+x_dis, piece.point.y);
        victim = getPiece(victim_point);
        removePiece(victim_point);
        addToGraveyard(victim);
        return victim;
    }

    private boolean moveIsCastling(Piece piece, Point dest)
    {
        int x_dis = dest.x - piece.point.x;
        int y_dis = dest.y - piece.point.y;
        King king = piece instanceof King ? (King)piece : null;

        return null != king && king.validCastling(x_dis, y_dis, this);
    }

    private void doCastling(Piece piece, Point dest) {
        int x_dis = dest.x - piece.point.x;
        int y;
        int x = piece.getPoint().x;
        if(getBottomColor() == piece.getColor())
            y = 7;
        else
            y = 0;
        if(x_dis < 0)
            move(getPiece(new Point(0,y)), new Point(x-1,y));
        else
            move(getPiece(new Point (7,y)), new Point(x+1,y));
    }

    private boolean moveIsDoubleMove(Piece piece, Point dest)
    {
        Piece victim = getPiece(dest);
        Pawn pawn = piece instanceof Pawn ? (Pawn)piece : null;
        int x_dis = dest.x - piece.getPoint().x;
        int y_dis = dest.y - piece.getPoint().y;
        return null != pawn && pawn.validDoubleMove(victim, x_dis, y_dis, this, dest);
    }

    private ArrayList<Piece> getThreats(Piece piece)
    {
        ArrayList<Piece> threats = new ArrayList<>();
        if(null == piece)
            return threats;

        for(int i=0; i<8; i++) {
            for (int j=0; j<8; j++) {
                Piece p = board[i][j];
                if(null != p && p.getColor() != piece.getColor() && p.validateMove(piece.getPoint(), this))
                    threats.add(p);
            }
        }
        return threats;
    }

    private King getKing(Color color)
    {
        if(Color.BLACK == color)
            return blackKing;
        else
            return whiteKing;
    }

    private void removePiece(Point point)
    {
        removePiece(point.x, point.y);
    }

    private void removePiece(int x, int y)
    {
        board[x][y] = null;
    }

    private void addToGraveyard(Piece victim)
    {
        if (victim.getColor() == Color.BLACK)
            capturedBlackPieces.add(victim);
        else
            capturedWhitePieces.add(victim);
    }

    private void removeFromGraveyard(Piece victim)
    {
        if (victim.getColor() == Color.BLACK)
            capturedBlackPieces.remove(victim);
        else
            capturedWhitePieces.remove(victim);
    }

    private void placePiece(Piece piece)
    {
        if(piece instanceof King) {
            if (piece.getColor() == Color.WHITE) {
                whiteKing = (King)piece;
            } else {
                blackKing = (King)piece;
            }
        }

        int x = piece.point.x;
        int y = piece.point.y;
        board[x][y] = piece;
    }

    private void undoLastMove(Piece victim, Piece moved, Point originalPoint) {
        move(moved, originalPoint);
        if(null != victim) {
            removeFromGraveyard(victim);
            placePiece(victim);
        }
    }

    private boolean outsideBounds(int x, int y)
    {
        return !(x >= 0 && x < 8 && y >= 0 && y < 8);
    }

    private void recordHistory(Point from, Point to) {
        Piece piece = Board.getInstance().getPiece(to);
        history = history + model.Color.toString(piece.getColor()) + " " + model.PieceType.toString(piece.getType()) + " from " +
                from.getX() + "," + from.getY() + " to " + to.getX() + "," + to.getY() + "\n";
        HistoryWindow.getInstance().updateHistory(history);
    }

    private void recordCapture(Piece capturedPiece, Piece capturingPiece) {
        history = history + model.Color.toString(capturingPiece.getColor()) + " " + model.PieceType.toString(capturingPiece.getType())
                + " captured " + model.Color.toString(capturedPiece.getColor()) + " "
                + model.PieceType.toString(capturedPiece.getType()) + "\n";
        HistoryWindow.getInstance().updateHistory(history);
    }

    private void recordPromotion(Piece piece) {
        history = history + model.Color.toString(piece.getColor()) + " pawn promoted to " + model.Color.toString(piece.getColor()) +
                " " + model.PieceType.toString(piece.getType()) + "\n";
        HistoryWindow.getInstance().updateHistory(history);
    }

    private void recordCheck(model.Color color) {
        history = history + model.Color.toString(color) + " is in check!\n";
        HistoryWindow.getInstance().updateHistory(history);
    }

    private void recordCheckmate(model.Color color) {
        history = history + model.Color.toString(color) + " has been defeated!";
        HistoryWindow.getInstance().updateHistory(history);
    }

    private void showMessage(String message, String title)
    {
        if(showMessages)
           GameWindow.getInstance().showMessage(message, title, JOptionPane.WARNING_MESSAGE);
    }

//endregion

}