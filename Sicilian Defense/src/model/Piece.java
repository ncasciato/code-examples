package model;

import java.awt.Point;
import java.util.ArrayList;

/**
 * Created by Tom on 7/27/2015.
 */
public abstract class Piece
{
    protected Color color;
    protected Point point;

    /**
     * @param color
     * @param point
     * Basic constructor
     */
    protected Piece(Color color, Point point)
    {
        this.color = color;
        this.point = point;
    }

    /**
     * @return color
     */
    public Color getColor()
    {
        return color;
    }

    /**
     * @return point
     */
    public Point getPoint()
    {
        return point;
    }

    /**
     * @param point
     */
    public void setPoint(Point point)
    {
        if(outsideBounds(point.x, point.y))
            throw new ArrayIndexOutOfBoundsException();
        this.point = point;
    }

    /**
     * @param point
     * @return outsideBound(x,y)
     */
    protected boolean outsideBounds(Point point)
    {
        return outsideBounds(point.x, point.y);
    }

    /**
     * @param x
     * @param y 
     * @return boolean based on bound location
     */
    protected boolean outsideBounds(int x, int y)
    {
        return !(x >= 0 && x < 8 && y >= 0 && y < 8);
    }

    /**
     * @param dest
     * @param board
     * @return
     * see if the dest is a possible move for this piece
     */
    public abstract boolean validateMove(Point dest, Board board);

    /**
     * @param piece
     * @param board
     * @return a list of points that's possible for attacking
     * this code is being used for check mate
     */
    public abstract ArrayList<Point> getAttackPath(Piece piece, Board board);

    /**
     * @return the type of the piece
     */
    public abstract PieceType getType();
}
