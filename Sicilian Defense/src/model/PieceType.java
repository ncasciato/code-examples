package model;

/**
 * Created by Tom on 7/27/2015.
 */
public enum PieceType {
    KING,QUEEN,ROOK,BISHOP,KNIGHT,PAWN;

    public static String toString(PieceType piece) {
        switch (piece) {
            case PAWN:
                return "pawn";
            case KNIGHT:
                return "knight";
            case BISHOP:
                return "bishop";
            case ROOK:
                return "rook";
            case QUEEN:
                return "queen";
            default:
                return "king";
        }
    }
}
