package model;

/**
 * Created by Tom on 7/27/2015.
 */
public enum Color {
    WHITE, BLACK;

    public static String toString(Color color) {
        switch (color) {
            case WHITE:
                return "white";
            default:
                return "black";
        }
    }
}
