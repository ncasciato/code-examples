package model;

import java.awt.Point;
import java.util.ArrayList;

/**
 * Created by Tom on 7/27/2015.
 */
public class King extends Piece {
    private boolean canCastle;

    /**
     * @param color
     * @param point
     * basic constructor
     */
    public King(Color color, Point point)
    {
        super(color, point);
        canCastle = true;
    }

    /* (non-Javadoc)
     * @see model.Piece#validateMove(java.awt.Point, model.Board)
     * check if the move is valid
     */
    public boolean validateMove(Point dest, Board board)
    {
        Piece victim = board.getPiece(dest);
        if(outsideBounds(dest))
            return false;
        else if(null != victim && victim.color == this.color)
            return false;

        int x_dis = dest.x - point.x;
        int y_dis = dest.y - point.y;

        if(validNormalMove(x_dis, y_dis))
            return true;

        return false;
    }

    /**
     * @param x_dis
     * @param y_dis
     * @return
     */
    private boolean validNormalMove(int x_dis, int y_dis)
    {
        return (1 == Math.abs(x_dis) || 0 == Math.abs(x_dis)) && (1 == Math.abs(y_dis) || 0 == Math.abs(y_dis));
    }

    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     * check if castling is allow based on the castling rule 
     */
    public boolean validCastling(int x_dis, int y_dis, Board board)
    {
        if(validLeftCastle(x_dis, y_dis, board))
            return true;
        else if(validRightCastle(x_dis, y_dis, board))
            return true;

        return false;
    }

    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     * check left castle
     */
    private boolean validLeftCastle(int x_dis, int y_dis, Board board)
    {
        if(-2 != x_dis || 0 != y_dis)
            return false;

        int x = this.getPoint().x;
        int y ;
        if(board.getBottomColor() == this.getColor())
            y = 7;
        else
            y = 0;
        // The king and the chosen rook are on the player's first rank.
        // Neither the king nor the chosen rook have previously moved.
        Rook rook = (Rook)board.getPiece(0,y);
        if(null == rook || !rook.canCastle() || !this.canCastle )
            return false;

        // There are no pieces between the king and the chosen rook.
        for (int i = x-1; i > 0; i--) {
            Piece p = board.getPiece(i, y);
            if (null != p)
                return false;
        }

        // The king is not currently in check.
        if(board.inCheck(this.color))
            return false;

        // The king does not pass through a square that is attacked by an enemy piece.
        for(int i = x-1; i > x-3; i--) {
            board.move(this, new Point(i, y));
            if(board.inCheck(this.color)) {
                board.move(this, new Point(x, y));
                return false;
            }
        }
        // The king does not end up in check. (True of any legal move.)
        board.move(rook, new Point(x-1, y));
        boolean inCheck = board.inCheck(this.color);
        board.move(rook, new Point(0, y));
        board.move(this, new Point(x, y));
        return !inCheck;
    }

    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     * check right castle
     */
    private boolean validRightCastle(int x_dis, int y_dis, Board board)
    {
        if(2 != x_dis || 0 != y_dis)
            return false;

        int y ;
        if(board.getBottomColor() == this.getColor())
            y = 7;
        else
            y = 0;
        int x = this.getPoint().x;
        // The king and the chosen rook are on the player's first rank.
        // Neither the king nor the chosen rook have previously moved.
        Rook rook = (Rook)board.getPiece(7, y);
        if(null == rook || !rook.canCastle() || !this.canCastle)
            return false;

        // There are no pieces between the king and the chosen rook.
        for (int i = x+1; i < 7; i++) {
            Piece p = board.getPiece(i, y);
            if (null != p)
                return false;
        }

        // The king is not currently in check.
        if(board.inCheck(this.color))
            return false;

        // The king does not pass through a square that is attacked by an enemy piece.
        for(int i = x+1; i < x+3; i++) {
            board.move(this, new Point(i, y));
            if(board.inCheck(this.color)) {
                board.move(this, new Point(x, y));
                return false;
            }
        }
        // The king does not end up in check. (True of any legal move.)
        board.move(rook, new Point(x+1, y));
        boolean inCheck = board.inCheck(this.color);
        board.move(rook, new Point(7, y));
        board.move(this, new Point(x, y));
        return !inCheck;
    }

    /* (non-Javadoc)
     * @see model.Piece#getAttackPath(model.Piece, model.Board)
     */
    public ArrayList<Point> getAttackPath(Piece victim, Board b)
    {
        ArrayList<Point> path = new ArrayList<>();
        int x_dis = victim.getPoint().x - point.x;
        int y_dis = victim.getPoint().y - point.y;
        if(validNormalMove(x_dis, y_dis))
            path.add(this.getPoint());
        return path;
    }

    /**
     * @param canIt
     */
    public void setCanCastle(boolean canIt) { canCastle = canIt; }

    /**
     * @return
     */
    public boolean canCastle()
    {
        return canCastle;
    }
    
    /* (non-Javadoc)
     * @see model.Piece#getType()
     */
    public PieceType getType()
    {
        return PieceType.KING;
    }
}
