package model;


import java.awt.Point;
import java.util.ArrayList;

/**
 * Created by Tom on 7/27/2015.
 * Edited by Sarah on 8/11/2015.
 */

public class Bishop extends Piece {

    /**
     * @param color
     * @param point
     * Basic Constructor
     */
    public Bishop(Color color, Point point)
    {
        super(color, point);
    }

    /* (non-Javadoc)
     * @see model.Piece#validateMove(java.awt.Point, model.Board)
     * Check if the point dest is a valid move based on the board
     */
    public boolean validateMove(Point dest, Board board)
    {
        Piece victim = board.getPiece(dest);
        if(outsideBounds(dest))
            return false;
        else if(null != victim && victim.color == color)
            return false;

        int x_dis = dest.x - point.x;
        int y_dis = dest.y - point.y;

        if(Math.abs(x_dis) != Math.abs(y_dis))
            return false;

        if(validUpLeftMove(x_dis, y_dis, board))
            return true;
        else if(validUpRightMove(x_dis, y_dis, board))
            return true;
        else if(validDownLeftMove(x_dis, y_dis, board))
            return true;
        else if(validDownRightMove(x_dis, y_dis, board))
            return true;

        return false;
    }
    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     * A helper function to see if upleft move is valid
     */
    private boolean validUpLeftMove(int x_dis, int y_dis, Board board)
    {
        if(x_dis > 0 || y_dis < 0)
            return false;

        for (int i = 1; i < y_dis; i++) {
            int x = point.x - i;
            int y = point.y + i;
            Piece p = board.getPiece(x, y);
            if (null != p)
                return false;
        }

        return true;
    }

    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     * A helper function to see if up right move is valid
     */
    private boolean validUpRightMove(int x_dis, int y_dis, Board board)
    {
        if(x_dis < 0 || y_dis < 0)
            return false;

        for (int i = 1; i < x_dis; i++) {
            int x = point.x + i;
            int y = point.y + i;
            Piece p = board.getPiece(x, y);
            if (null != p)
                return false;
        }

        return true;
    }

    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     * A helper function to see if down left move is valid
     */
    private boolean validDownLeftMove(int x_dis, int y_dis, Board board)
    {
        if(x_dis > 0 || y_dis > 0)
            return false;

        for (int i = -1; i > x_dis; i--) {
            int x = point.x + i;
            int y = point.y + i;
            Piece p = board.getPiece(x, y);
            if (null != p)
                return false;
        }

        return true;
    }

    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     * A helper function to see if down right move is valid
     */
    private boolean validDownRightMove(int x_dis, int y_dis, Board board)
    {
        if(x_dis < 0 || y_dis > 0)
            return false;

        for (int i = 1; i < x_dis; i++) {
            int x = point.x + i;
            int y = point.y - i;
            Piece p = board.getPiece(x, y);
            if (null != p)
                return false;
        }

        return true;
    }

    /* (non-Javadoc)
     * @see model.Piece#getAttackPath(model.Piece, model.Board)
     */
    public ArrayList<Point> getAttackPath(Piece victim, Board board)
    {
        int x_dis = victim.getPoint().x - this.getPoint().x;
        int y_dis = victim.getPoint().y - this.getPoint().y;

        if(Math.abs(x_dis) != Math.abs(y_dis) || x_dis == 0)
            return new ArrayList<>(); //not in bishops path

        if(x_dis < 0 && y_dis > 0)
            return getUpLeftPath(y_dis);
        else if(x_dis > 0 && y_dis > 0)
            return getUpRightPath(x_dis);
        else if(x_dis < 0 && y_dis < 0)
            return getDownLeftPath(x_dis);
        else //if(x_dis > 0 && y_dis < 0)
            return getDownRightPath(x_dis);
    }

    /**
     * @param dis
     * @return
     */
    private ArrayList<Point> getUpLeftPath(int dis)
    {
        ArrayList<Point> path = new ArrayList<>();
        for (int i = 0; i < dis; i++) {
            int x = point.x - i;
            int y = point.y + i;
            Point p = new Point(x,y);
            path.add(p);
        }
        return path;
    }

    /**
     * @param dis
     * @return
     */
    private ArrayList<Point> getUpRightPath(int dis)
    {
        ArrayList<Point> path = new ArrayList<>();
        for (int i = 0; i < dis; i++) {
            int x = point.x + i;
            int y = point.y + i;
            Point p = new Point(x,y);
            path.add(p);
        }
        return path;
    }

    /**
     * @param dis
     * @return
     */
    private ArrayList<Point> getDownLeftPath(int dis)
    {
        ArrayList<Point> path = new ArrayList<>();
        for (int i = 0; i > dis; i--) {
            int x = point.x + i;
            int y = point.y + i;
            Point p = new Point(x,y);
            path.add(p);
        }
        return path;
    }

    /**
     * @param dis
     * @return
     */
    private ArrayList<Point> getDownRightPath(int dis)
    {
        ArrayList<Point> path = new ArrayList<>();
        for (int i = 0; i < dis; i++) {
            int x = point.x + i;
            int y = point.y - i;
            Point p = new Point(x,y);
            path.add(p);
        }
        return path;
    }


    /* (non-Javadoc)
     * @see model.Piece#getType()
     * return type
     */
    public PieceType getType()
    {
        return PieceType.BISHOP;
    }
}
