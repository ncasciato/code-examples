package model;

import java.awt.Point;
import java.util.ArrayList;

/**
 * Created by Tom on 7/27/2015.
 */
public class Pawn extends Piece {
    private boolean firstMove;
    private boolean canBeEnPassant;

    /**
     * @param color
     * @param point
     */
    public Pawn(Color color, Point point)
    {
        super(color, point);
        firstMove = true;
    }

    /* (non-Javadoc)
     * @see model.Piece#validateMove(java.awt.Point, model.Board)
     */
    public boolean validateMove(Point dest, Board board)
    {
        Piece victim = board.getPiece(dest);
        if(outsideBounds(dest))
            return false;
        else if(null != victim && victim.color == color)
            return false;

        int x_dis = dest.x - point.x;
        int y_dis = dest.y - point.y;

        if(validNormalMove(victim, x_dis, y_dis, board))
            return true;
        else if(validDoubleMove(victim, x_dis, y_dis, board, dest))
            return true;
        else if(validAttack(victim, x_dis,y_dis, board))
            return true;

        return false;
    }

    /**
     * @param victim
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     * Validate that a normal move is possible
     */
    private boolean validNormalMove(Piece victim, int x_dis, int y_dis, Board board)
    {
        int direction;
        if(board.getBottomColor() == this.color)
            direction = -1;
        else
            direction = 1;

        return null == victim && 0 == x_dis && direction == y_dis;
    }

    /**
     * @param victim
     * @param x_dis
     * @param y_dis
     * @param board
     * @param dest
     * @return
     * The first move of a pawn can be a double move
     */
    public boolean validDoubleMove(Piece victim, int x_dis, int y_dis, Board board, Point dest)
    {
        int direction;
        Piece p;
        if(board.getBottomColor() == this.color) {
            p = board.getPiece(dest.x, dest.y+1);
            direction = -2;
        }
        else {
            p = board.getPiece(dest.x, dest.y-1);
            direction = 2;
        }

        return firstMove && null == victim && null == p && 0 == x_dis && direction == y_dis;
    }

    /**
     * @param victim
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     */
    private boolean validAttack(Piece victim, int x_dis, int y_dis, Board board)
    {
        int direction;
        if(board.getBottomColor() == this.color)
            direction = -1;
        else
            direction = 1;

        return null != victim && 1 == Math.abs(x_dis) && direction == y_dis;
    }

    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     * Check if we can enpassant
     */
    public boolean validEnPassant(int x_dis, int y_dis, Board board)
    {
        int direction;
        if(board.getBottomColor() == this.color)
            direction = -1;
        else
            direction = 1;

        if(1 == Math.abs(x_dis) && direction == y_dis)
        {
            Piece p = board.getPiece(point.x+x_dis, point.y);
            if(p instanceof Pawn && ((Pawn)p).canBeEnPassant() && this.color != p.getColor())
                return true;
        }

        return false;
    }

    /* (non-Javadoc)
     * @see model.Piece#getAttackPath(model.Piece, model.Board)
     */
    public ArrayList<Point> getAttackPath(Piece victim, Board board)
    {
        ArrayList<Point> path = new ArrayList<>();
        int x_dis = victim.getPoint().x - point.x;
        int y_dis = victim.getPoint().y - point.y;
        if(validAttack(victim, x_dis, y_dis, board))
            path.add(this.getPoint());

        return path;
    }

    /**
     * @param board
     * @return
     * Check if we can be promoted (at 8th space of the board)
     */
    public boolean canBePromoted(Board board)
    {
        int end_y;
        if(board.getBottomColor() == this.color)
            end_y = 0;
        else
            end_y = 7;

        return this.getPoint().y == end_y;

    }

    /**
     * @param isIt
     */
    public void setFirstMove(boolean isIt) { firstMove = isIt; }

    /**
     * @param canIt
     */
    public void setCanBeEnPassant(boolean canIt)
    {
        canBeEnPassant = canIt;
    }

    /**
     * @return canBeEnPassant
     */
    public boolean canBeEnPassant()
    {
        return canBeEnPassant;
    }

    /* (non-Javadoc)
     * @see model.Piece#getType()
     */
    public PieceType getType()
    {
        return PieceType.PAWN;
    }
}
