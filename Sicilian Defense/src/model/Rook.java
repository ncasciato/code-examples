package model;

import java.awt.Point;
import java.util.ArrayList;

/**
 * Created by Tom on 7/27/2015.
 */
public class Rook extends Piece {
    private boolean canCastle;

    /**
     * @param color
     * @param point
     */
    public Rook(Color color, Point point)
    {
        super(color, point);
        canCastle = true;
    }

    /* (non-Javadoc)
     * @see model.Piece#validateMove(java.awt.Point, model.Board)
     */
    public boolean validateMove(Point dest, Board board)
    {
        Piece victim = board.getPiece(dest);
        if(outsideBounds(dest))
            return false;
        else if(null != victim && victim.color == color)
            return false;

        int x_dis = dest.x - point.x;
        int y_dis = dest.y - point.y;

        if(validLeftMove(x_dis, y_dis, board))
            return true;
        else if(validRightMove(x_dis, y_dis, board))
            return true;
        else if(validUpMove(x_dis, y_dis, board))
            return true;
        else if(validDownMove(x_dis, y_dis, board))
            return true;

        return false;
    }

    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     */
    private boolean validLeftMove(int x_dis, int y_dis, Board board)
    {
        if(0 != y_dis || 0 <= x_dis)
            return false;

        int y = point.y;
        for (int i = -1; i > x_dis; i--) {
            int x = point.x + i;
            Piece p = board.getPiece(x, y);
            if (null != p)
                return false;
        }

        return true;
    }

    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     */
    private boolean validRightMove(int x_dis, int y_dis, Board board)
    {
        if(0 != y_dis || 0 >= x_dis)
            return false;

        int y = point.y;
        for (int i = 1; i < x_dis; i++) {
            int x = point.x + i;
            Piece p = board.getPiece(x, y);
            if (null != p)
                return false;
        }

        return true;
    }

    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     */
    private boolean validUpMove(int x_dis, int y_dis, Board board)
    {
        if(0 >= y_dis || 0 != x_dis)
            return false;

        int x = point.x;
        for (int i = 1; i < y_dis; i++) {
            int y = point.y + i;
            Piece p = board.getPiece(x, y);
            if (null != p)
                return false;
        }

        return true;
    }

    /**
     * @param x_dis
     * @param y_dis
     * @param board
     * @return
     */
    private boolean validDownMove(int x_dis, int y_dis, Board board)
    {
        if(0 <= y_dis || 0 != x_dis)
            return false;

        int x = point.x;
        for (int i = -1; i > y_dis; i--) {
            int y = point.y + i;
            Piece p = board.getPiece(x, y);
            if (null != p)
                return false;
        }

        return true;
    }

    /* (non-Javadoc)
     * @see model.Piece#getAttackPath(model.Piece, model.Board)
     */
    public ArrayList<Point> getAttackPath(Piece victim, Board board)
    {
        int x_dis = victim.getPoint().x - this.getPoint().x;
        int y_dis = victim.getPoint().y - this.getPoint().y;

        if(0 == y_dis && 0 > x_dis)
            return getLeftPath(x_dis);
        else if(0 == y_dis && 0 < x_dis)
            return getRightPath(x_dis);
        else if(0 < y_dis && 0 == x_dis)
            return getUpPath(y_dis);
        else if(0 > y_dis && 0 == x_dis)
            return getDownPath(y_dis);

        return new ArrayList<>();
    }

    /**
     * @param x_dis
     * @return
     */
    private ArrayList<Point> getLeftPath(int x_dis)
    {
        ArrayList<Point> path = new ArrayList<>();
        int y = point.y;
        for (int i = 0; i > x_dis; i--) {
            int x = point.x + i;
            Point p = new Point(x, y);
            path.add(p);
        }
        return path;
    }

    /**
     * @param x_dis
     * @return
     */
    private ArrayList<Point> getRightPath(int x_dis)
    {
        ArrayList<Point> path = new ArrayList<>();
        int y = point.y;
        for (int i = 0; i < x_dis; i++) {
            int x = point.x + i;
            Point p = new Point(x, y);
            path.add(p);
        }
        return path;
    }

    /**
     * @param y_dis
     * @return
     */
    private ArrayList<Point> getUpPath(int y_dis)
    {
        ArrayList<Point> path = new ArrayList<>();
        int x = point.x;
        for (int i = 0; i < y_dis; i++) {
            int y = point.y + i;
            Point p = new Point(x, y);
            path.add(p);
        }
        return path;
    }

    /**
     * @param y_dis
     * @return
     */
    private ArrayList<Point> getDownPath(int y_dis)
    {
        ArrayList<Point> path = new ArrayList<>();
        int x = point.x;
        for (int i = 0; i > y_dis; i--) {
            int y = point.y + i;
            Point p = new Point(x, y);
            path.add(p);
        }
        return path;
    }

    /**
     * @param canIt
     */
    public void setCanCastle(boolean canIt) { canCastle = canIt; }

    /**
     * @return
     */
    public boolean canCastle()
    {
        return canCastle;
    }

    /* (non-Javadoc)
     * @see model.Piece#getType()
     */
    public PieceType getType()
    {
        return PieceType.ROOK;
    }
}
