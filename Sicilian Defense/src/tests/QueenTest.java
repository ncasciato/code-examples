package tests;

import model.*;
import model.Color;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by Tom on 8/17/2015.
 */
public class QueenTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testValidateMove_Right() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Queen q = new Queen(Color.WHITE, new Point(0,3));
        Pawn p1 = new Pawn(Color.BLACK, new Point(4,3));
        Pawn p2 = new Pawn(Color.WHITE, new Point(6,3));

        TestsHelper.placeOnBoard(b,q);
        Assert.assertTrue(b.movePiece(q, new Point(7,3)));
        b.forceMovePiece(q, new Point(0,3));

        TestsHelper.placeOnBoard(b,p1);
        Assert.assertFalse(b.movePiece(q, new Point(5,3)));
        Assert.assertTrue(b.movePiece(q, new Point(4,3)));
        Assert.assertFalse(b.movePiece(q, new Point(8,3)));

        TestsHelper.placeOnBoard(b,p2);
        Assert.assertFalse(b.movePiece(q, new Point(7,3)));
        Assert.assertFalse(b.movePiece(q, new Point(6,3)));
    }

    @Test
    public void testValidateMove_Left() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Queen q = new Queen(Color.WHITE, new Point(7,3));
        Pawn p1 = new Pawn(Color.BLACK, new Point(3,3));
        Pawn p2 = new Pawn(Color.WHITE, new Point(1,3));

        TestsHelper.placeOnBoard(b,q);
        Assert.assertTrue(b.movePiece(q, new Point(0,3)));
        b.forceMovePiece(q, new Point(7,3));

        TestsHelper.placeOnBoard(b,p1);
        Assert.assertFalse(b.movePiece(q, new Point(2,3)));
        Assert.assertTrue(b.movePiece(q, new Point(3,3)));
        Assert.assertFalse(b.movePiece(q, new Point(8,3)));

        TestsHelper.placeOnBoard(b,p2);
        Assert.assertFalse(b.movePiece(q, new Point(0,3)));
        Assert.assertFalse(b.movePiece(q, new Point(1,3)));
    }

    @Test
    public void testValidateMove_Up() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Queen q = new Queen(Color.WHITE, new Point(3,0));
        Pawn p1 = new Pawn(Color.BLACK, new Point(3,4));
        Pawn p2 = new Pawn(Color.WHITE, new Point(3,6));

        TestsHelper.placeOnBoard(b,q);
        Assert.assertTrue(b.movePiece(q, new Point(3,7)));
        b.forceMovePiece(q, new Point(3,0));

        TestsHelper.placeOnBoard(b,p1);
        Assert.assertFalse(b.movePiece(q, new Point(3,5)));
        Assert.assertTrue(b.movePiece(q, new Point(3,4)));
        Assert.assertFalse(b.movePiece(q, new Point(3,8)));

        TestsHelper.placeOnBoard(b,p2);
        Assert.assertFalse(b.movePiece(q, new Point(3,7)));
        Assert.assertFalse(b.movePiece(q, new Point(3,6)));
    }

    @Test
    public void testValidateMove_Down() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Queen q = new Queen(Color.WHITE, new Point(3,7));
        Pawn p1 = new Pawn(Color.BLACK, new Point(3,3));
        Pawn p2 = new Pawn(Color.WHITE, new Point(3,1));

        TestsHelper.placeOnBoard(b,q);
        Assert.assertTrue(b.movePiece(q, new Point(3,0)));
        b.forceMovePiece(q, new Point(3,7));

        TestsHelper.placeOnBoard(b,p1);
        Assert.assertFalse(b.movePiece(q, new Point(3,2)));
        Assert.assertTrue(b.movePiece(q, new Point(3,3)));
        Assert.assertFalse(b.movePiece(q, new Point(3,-1)));

        TestsHelper.placeOnBoard(b,p2);
        Assert.assertFalse(b.movePiece(q, new Point(3,0)));
        Assert.assertFalse(b.movePiece(q, new Point(3,1)));
    }
    @Test
    public void testValidateMove_UpLeft() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Queen q = new Queen(Color.WHITE, new Point(7,0));
        Pawn p1 = new Pawn(Color.BLACK, new Point(4,3));
        Pawn p2 = new Pawn(Color.WHITE, new Point(1,6));

        TestsHelper.placeOnBoard(b,q);
        Assert.assertTrue(b.movePiece(q, new Point(0,7)));
        b.forceMovePiece(q, new Point(7,0));

        TestsHelper.placeOnBoard(b,p1);
        Assert.assertFalse(b.movePiece(q, new Point(3,4)));
        Assert.assertTrue(b.movePiece(q, new Point(4,3)));
        Assert.assertFalse(b.movePiece(q, new Point(-1,8)));

        TestsHelper.placeOnBoard(b,p2);
        Assert.assertFalse(b.movePiece(q, new Point(0,7)));
        Assert.assertFalse(b.movePiece(q, new Point(1,6)));
    }

    @Test
    public void testValidateMove_UpRight() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Queen q = new Queen(Color.WHITE, new Point(0,0));
        Pawn p1 = new Pawn(Color.BLACK, new Point(3,3));
        Pawn p2 = new Pawn(Color.WHITE, new Point(6,6));

        TestsHelper.placeOnBoard(b,q);
        Assert.assertTrue(b.movePiece(q, new Point(7,7)));
        b.forceMovePiece(q, new Point(0,0));

        TestsHelper.placeOnBoard(b,p1);
        Assert.assertFalse(b.movePiece(q, new Point(4,4)));
        Assert.assertTrue(b.movePiece(q, new Point(3,3)));
        Assert.assertFalse(b.movePiece(q, new Point(8,8)));

        TestsHelper.placeOnBoard(b,p2);
        Assert.assertFalse(b.movePiece(q, new Point(7,7)));
        Assert.assertFalse(b.movePiece(q, new Point(6,6)));
    }

    @Test
    public void testValidateMove_DownLeft() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Queen q = new Queen(Color.WHITE, new Point(7,7));
        Pawn p1 = new Pawn(Color.BLACK, new Point(4,4));
        Pawn p2 = new Pawn(Color.WHITE, new Point(1,1));

        TestsHelper.placeOnBoard(b,q);
        Assert.assertTrue(b.movePiece(q, new Point(0,0)));
        b.forceMovePiece(q, new Point(7,7));

        TestsHelper.placeOnBoard(b,p1);
        Assert.assertFalse(b.movePiece(q, new Point(3,3)));
        Assert.assertTrue(b.movePiece(q, new Point(4,4)));
        Assert.assertFalse(b.movePiece(q, new Point(-1,-1)));

        TestsHelper.placeOnBoard(b,p2);
        Assert.assertFalse(b.movePiece(q, new Point(0,0)));
        Assert.assertFalse(b.movePiece(q, new Point(1,1)));
    }

    @Test
    public void testValidateMove_DownRight() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Queen q = new Queen(Color.WHITE, new Point(0,7));
        Pawn p1 = new Pawn(Color.BLACK, new Point(3,4));
        Pawn p2 = new Pawn(Color.WHITE, new Point(6,1));

        TestsHelper.placeOnBoard(b,q);
        Assert.assertTrue(b.movePiece(q, new Point(7,0)));
        b.forceMovePiece(q, new Point(0,7));

        TestsHelper.placeOnBoard(b,p1);
        Assert.assertFalse(b.movePiece(q, new Point(4,3)));
        Assert.assertTrue(b.movePiece(q, new Point(3,4)));
        Assert.assertFalse(b.movePiece(q, new Point(8,-1)));

        TestsHelper.placeOnBoard(b,p2);
        Assert.assertFalse(b.movePiece(q, new Point(7,0)));
        Assert.assertFalse(b.movePiece(q, new Point(6,1)));
    }

    @Test
    public void testGetAttackPath() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Queen q = new Queen(Color.BLACK, new Point(3,3));
        Knight k1 = new Knight(Color.WHITE, new Point(2, 3));
        Knight k2 = new Knight(Color.WHITE, new Point(3, 1));
        Knight k3 = new Knight(Color.WHITE, new Point(6, 3));
        Knight k4 = new Knight(Color.WHITE, new Point(3, 7));
        Knight k5 = new Knight(Color.WHITE, new Point(3, 3));

        Rook rook = new Rook(Color.WHITE, new Point(2,2));
        Pawn pawn = new Pawn(Color.WHITE, new Point(1,5));
        Knight knight = new Knight(Color.WHITE, new Point(6,6));
        Queen queen = new Queen(Color.WHITE, new Point(0,6));
        King king = new King(Color.WHITE, new Point(1,2));

        TestsHelper.placeOnBoard(b, q);
        TestsHelper.placeOnBoard(b, k1);
        TestsHelper.placeOnBoard(b, k2);
        TestsHelper.placeOnBoard(b, k3);
        TestsHelper.placeOnBoard(b, k4);
        TestsHelper.placeOnBoard(b, k5);
        TestsHelper.placeOnBoard(b, rook);
        TestsHelper.placeOnBoard(b, pawn);
        TestsHelper.placeOnBoard(b, knight);
        TestsHelper.placeOnBoard(b, queen);
        TestsHelper.placeOnBoard(b, king);

        ArrayList<Point> path1 = q.getAttackPath(k1, b);
        ArrayList<Point> path2 = q.getAttackPath(k2, b);
        ArrayList<Point> path3 = q.getAttackPath(k3, b);
        ArrayList<Point> path4 = q.getAttackPath(k4, b);
        ArrayList<Point> path5 = q.getAttackPath(k5, b);
        ArrayList<Point> rookPath = q.getAttackPath(rook, b);
        ArrayList<Point> pawnPath = q.getAttackPath(pawn, b);
        ArrayList<Point> queenPath = q.getAttackPath(queen, b);
        ArrayList<Point> knightPath = q.getAttackPath(knight, b);
        ArrayList<Point> kingPath = q.getAttackPath(king, b);

        Assert.assertEquals(1, path1.size());
        Assert.assertEquals(2, path2.size());
        Assert.assertEquals(3, path3.size());
        Assert.assertEquals(4, path4.size());
        Assert.assertEquals(0, path5.size());
        Assert.assertEquals(1, rookPath.size());
        Assert.assertEquals(2, pawnPath.size());
        Assert.assertEquals(3, knightPath.size());
        Assert.assertEquals(3, queenPath.size());
        Assert.assertEquals(0, kingPath.size());
    }

    @Test
    public void testGetType() throws Exception {
        Queen p = new Queen(Color.BLACK, new Point(0,1));
        Assert.assertEquals(p.getType(), PieceType.QUEEN);
    }
}