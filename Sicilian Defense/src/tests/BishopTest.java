package tests;

import model.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.awt.Point;
import java.util.ArrayList;

/**
 * Created by sarahcuda on 8/17/15.
 */
public class BishopTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testValidateMoves() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Bishop bishop = new Bishop(Color.BLACK, new Point(3,1));
        TestsHelper.placeOnBoard(b, bishop);

        Assert.assertTrue(b.movePiece(bishop, new Point(5,3)));
        Assert.assertTrue(b.movePiece(bishop, new Point(3,5)));
        Assert.assertTrue(b.movePiece(bishop, new Point(1,3)));
        Assert.assertTrue(b.movePiece(bishop, new Point(3,1)));

        Assert.assertFalse(b.movePiece(bishop, new Point(5,1)));
        Assert.assertFalse(b.movePiece(bishop, new Point(2,1)));
        Assert.assertFalse(b.movePiece(bishop, new Point(3,0)));
        Assert.assertFalse(b.movePiece(bishop, new Point(3,3)));
    }

    @Test
    public void testMoveThrough() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Bishop bishop = new Bishop(Color.BLACK, new Point(3,2));
        Rook rook = new Rook(Color.BLACK, new Point(4,3));
        Rook rook2 = new Rook(Color.BLACK, new Point(2,3));
        King king = new King(Color.BLACK, new Point(2,1));
        Queen queen = new Queen(Color.BLACK, new Point(4,1));

        TestsHelper.placeOnBoard(b, bishop);
        TestsHelper.placeOnBoard(b, rook);
        TestsHelper.placeOnBoard(b, rook2);
        TestsHelper.placeOnBoard(b, king);
        TestsHelper.placeOnBoard(b, queen);

        Assert.assertFalse(b.movePiece(bishop, new Point(5, 4)));
        Assert.assertFalse(b.movePiece(bishop, new Point(1,4)));
        Assert.assertFalse(b.movePiece(bishop, new Point(1,0)));
        Assert.assertFalse(b.movePiece(bishop, new Point(5,0)));
        Assert.assertFalse(b.movePiece(bishop, new Point(4,3)));
    }

    @Test
    public void testAttacks() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Bishop bishop = new Bishop(Color.BLACK, new Point(3,1));
        Rook rook = new Rook(Color.WHITE, new Point(4,2));
        Pawn pawn = new Pawn(Color.WHITE, new Point(2,0));
        Rook rook2 = new Rook(Color.WHITE, new Point(4,0));
        Queen queen = new Queen(Color.WHITE, new Point(1,3));

        TestsHelper.placeOnBoard(b, bishop);
        TestsHelper.placeOnBoard(b, rook);
        TestsHelper.placeOnBoard(b, pawn);
        TestsHelper.placeOnBoard(b, rook2);
        TestsHelper.placeOnBoard(b, queen);

        Assert.assertTrue(b.movePiece(bishop, new Point(4, 2)));
        Assert.assertTrue(b.movePiece(bishop, new Point(3, 1)));
        Assert.assertTrue(b.movePiece(bishop, new Point(2, 0)));
        Assert.assertTrue(b.movePiece(bishop, new Point(3, 1)));
        Assert.assertTrue(b.movePiece(bishop, new Point(4, 0)));
        Assert.assertTrue(b.movePiece(bishop, new Point(3, 1)));
        Assert.assertTrue(b.movePiece(bishop, new Point(1, 3)));
    }

    @Test
    public void testGetAttackPath() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Bishop bishop = new Bishop(Color.BLACK, new Point(4,3));
        Rook rook = new Rook(Color.WHITE, new Point(5,4));
        Pawn pawn = new Pawn(Color.WHITE, new Point(1,0));
        Knight knight = new Knight(Color.WHITE, new Point(6,1));
        Queen queen = new Queen(Color.WHITE, new Point(0,7));
        King king = new King(Color.WHITE, new Point(0, 3));

        TestsHelper.placeOnBoard(b, bishop);
        TestsHelper.placeOnBoard(b, rook);
        TestsHelper.placeOnBoard(b, pawn);
        TestsHelper.placeOnBoard(b, knight);
        TestsHelper.placeOnBoard(b, queen);
        TestsHelper.placeOnBoard(b, king);

        ArrayList<Point> rookPath = bishop.getAttackPath(rook, b);
        ArrayList<Point> pawnPath = bishop.getAttackPath(pawn, b);
        ArrayList<Point> queenPath = bishop.getAttackPath(queen, b);
        ArrayList<Point> knightPath = bishop.getAttackPath(knight, b);
        ArrayList<Point> kingPath = bishop.getAttackPath(king, b);

        Assert.assertEquals(1, rookPath.size());
        Assert.assertEquals(2, knightPath.size());
        Assert.assertEquals(3, pawnPath.size());
        Assert.assertEquals(4, queenPath.size());
        Assert.assertEquals(0, kingPath.size());
    }

    @Test
    public void testOutOfBounds() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Bishop bishop = new Bishop(Color.BLACK, new Point(3,1));
        TestsHelper.placeOnBoard(b, bishop);

        Assert.assertFalse(b.movePiece(bishop, new Point(-1, 4)));
        Assert.assertFalse(b.movePiece(bishop, new Point(8, 5)));
        Assert.assertFalse(b.movePiece(bishop, new Point(1, -1)));
        Assert.assertFalse(b.movePiece(bishop, new Point(5, -1)));
    }

    @Test
    public void testGetType() throws Exception {
        Bishop b = new Bishop(Color.BLACK, new Point(0,2));
        Assert.assertEquals(b.getType(), PieceType.BISHOP);
    }

}
