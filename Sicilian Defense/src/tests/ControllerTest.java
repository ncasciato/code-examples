package tests;
import java.awt.Point;
import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.reflect.*;

import org.junit.*;

import controller.Controller;
import controller.NetworkMode;
import controller.SDMessage;
import controller.SDMessageType;
import model.Board;

public class ControllerTest {
	
	
	// Reflection helpers because I don't want to add extra public
	// members or methods just for testing
	

    @After
    public void tearDown() throws Exception {
    	//destroy the connection
    }
    
	public static boolean controllerIsMyTurn(Controller controller)
	{
		try
		{
			Field moveReceivedField = controller.getClass().getDeclaredField("myTurn");
			moveReceivedField.setAccessible(true);
			return moveReceivedField.getBoolean(controller);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public static void doControllerMoveCycle(Controller controller)
	{
		if (controllerIsMyTurn(controller))
		{
			controller.makeMove(new Point(0,1), new Point(0,3));
		}
		
	}
	
	@Test
	public void testGetInstance()
	{
		Controller controller = Controller.getInstance();
		Assert.assertNotNull(controller);
	}

	
	/*@Test
	public void testGetServerAddresses()
	{
		Controller controller = getControllerTestingInstance(null);
		Assert.assertNotEquals("", controller.getServerIP());
	}*/
	
	private boolean serverGoFirst = false;
	private boolean	clientGoFirst = false;

	private int port = 1234;
	@Test
	public void testConnection(){
		testConnectionAndMoves(port);
		port++;

	}
	
	
 public Controller serverController;
	public void testConnectionAndMoves(final int port)
	{
        
      Thread clientThread =   (new Thread(){
        	public void run(){
        		Controller clientController = Controller.getInstance();
        		Assert.assertNotNull(clientController);
        		
        		// Wait a bit for server to initialize
        		try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
        		clientController.initialize(NetworkMode.CLIENT, "localhost", 1234);

        		try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
        		Assert.assertTrue(clientController.isConnected());
        		
        		// Wait for the init message
        		System.out.println("Client Init received: " + clientController.serverInitReceived());
        		while (!clientController.myTurn())
        		{
        			try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
        		}	
        		// Wait for the init message
        		System.out.println("Client Init received: " + clientController.serverInitReceived());
            	
        		
        		doControllerMoveCycle(clientController);
        		System.out.println("Client move");
        		if(!clientController.moveReceived())
        		{
                	clientGoFirst=true;
        			Assert.assertNotNull(Board.getInstance().getPiece(new Point(0,3)));
        		}
        		else
        		{
            		Assert.assertNotNull(Board.getInstance().getPiece(new Point(7,4)));
        		}
        	}
        });
      clientThread.start();
        
        serverController = Controller.getInstance();
        Assert.assertNotNull(serverController);
        
        serverController.initialize(NetworkMode.SERVER, "localhost", 1234);
        System.out.println("Waiting for client to connect");
        //Waiting for Client to connect
        while (!serverController.clientInitConfrirmed())
        {
        	try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }

		
		// Wait for the init message
		System.out.println("server Init received: " + serverController.clientInitConfrirmed());

        while (!serverController.myTurn())
        {
        	try {

				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
        
        
        doControllerMoveCycle(serverController);
		System.out.println("Server move");
        if(!serverController.moveReceived())
        {
        	serverGoFirst=true;
        	Assert.assertNotNull(Board.getInstance().getPiece(new Point(0,3)));
        }
        else
            Assert.assertNotNull(Board.getInstance().getPiece(new Point(7,4)));
       try {
		clientThread.join();
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
       File file = new File("test.txt");
       serverController.saveFile(file);
       Assert.assertNotNull(file);
       
       //testing restart here since we have the server and client set up.
       //this tests that the piece at 7,4 is no longer there.
       //game got restarted
		serverController.restart();
        Assert.assertNull(Board.getInstance().getPiece(new Point(7,4)));
        	
	}
	
	

	
	
}
