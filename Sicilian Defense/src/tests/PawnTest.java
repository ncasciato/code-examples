package tests;

import model.*;
import model.Color;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.*;

/**
 * Created by Tom on 8/11/2015.
 */
public class PawnTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testValidateMove_NormalForward() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Assert.assertTrue(b.movePiece(new Point(0,1), new Point(0,2)));
        Assert.assertFalse(b.movePiece(new Point(0, 2), new Point(0, 1)));
        Assert.assertFalse(b.movePiece(new Point(0, 2), new Point(1, 3)));
    }

    @Test
    public void testValidateMove_NormalBackward() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Assert.assertTrue(b.movePiece(new Point(4, 6), new Point(4, 5)));
        Assert.assertFalse(b.movePiece(new Point(4, 5), new Point(4, 6)));
        Assert.assertFalse(b.movePiece(new Point(4, 5), new Point(3, 4)));
    }

    @Test
    public void testValidateMove_DoubleForward() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Pawn p = (Pawn)b.getPiece(0,1);
        Assert.assertTrue(b.movePiece(p, new Point(0, 3)));
        Assert.assertFalse(b.movePiece(p, new Point(0, 5)));
    }

    @Test
    public void testValidateMove_DoubleBackward() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Pawn p2 = (Pawn)b.getPiece(1,6);
        Assert.assertTrue(b.movePiece(p2, new Point(1, 4)));
        Assert.assertFalse(b.movePiece(p2, new Point(1,2)));
    }

    @Test
    public void testValidateMove_AttackForward() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Pawn p = (Pawn)b.getPiece(1,1);
        b.forceMovePiece(new Point(0,6), new Point(0,4));
        b.forceMovePiece(new Point(2,6), new Point(2,4));
        b.forceMovePiece(p, new Point(1,3));

        // attack
        Assert.assertTrue(b.movePiece(p, new Point(0,4)));
        b.forceMovePiece(p, new Point(1,3));
        Assert.assertTrue(b.movePiece(p, new Point(2, 4)));

        // only move that way when attacking
        Assert.assertFalse(b.movePiece(p, new Point(1, 5)));
        Assert.assertFalse(b.movePiece(p, new Point(3, 5)));

        // can't attack friends
        Pawn p_friend1 = (Pawn)b.getPiece(0,1);
        Pawn p_friend2 = (Pawn)b.getPiece(2,1);
        b.forceMovePiece(p_friend1, new Point(1,5));
        b.forceMovePiece(p_friend2, new Point(3,5));
        Assert.assertFalse(b.movePiece(p, new Point(1, 5)));
        Assert.assertFalse(b.movePiece(p, new Point(3, 5)));
    }

    @Test
    public void testValidateMove_AttackBackward() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Pawn p2 = (Pawn)b.getPiece(5, 6);
        b.forceMovePiece(new Point(6, 1), new Point(6, 3));
        b.forceMovePiece(new Point(4, 1), new Point(4, 3));
        b.forceMovePiece(p2, new Point(5, 4));

        // attack
        Assert.assertTrue(b.movePiece(p2, new Point(6, 3)));
        b.forceMovePiece(p2, new Point(5, 4));
        Assert.assertTrue(b.movePiece(p2, new Point(4, 3)));

        // only move that way when attacking
        Assert.assertFalse(b.movePiece(p2, new Point(3, 2)));
        Assert.assertFalse(b.movePiece(p2, new Point(5, 2)));

        // can't attack friends
        Pawn p2_friend1 = (Pawn)b.getPiece(6, 6);
        Pawn p2_friend2 = (Pawn)b.getPiece(4, 6);
        b.forceMovePiece(p2_friend1, new Point(3, 2));
        b.forceMovePiece(p2_friend2, new Point(5, 2));
        Assert.assertFalse(b.movePiece(p2, new Point(2, 2)));
        Assert.assertFalse(b.movePiece(p2, new Point(5, 2)));
    }

    @Test
    public void testValidateMove_EnPassantForward() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Pawn p = (Pawn)b.getPiece(1, 1);
        b.forceMovePiece(p, new Point(1, 4));
        b.forceMovePiece(new Point(0, 6), new Point(0, 4));
        b.forceMovePiece(new Point(2, 6), new Point(2, 4));
        Pawn p2 = (Pawn)b.getPiece(new Point(0,4));
        Pawn p3 = (Pawn)b.getPiece(new Point(2,4));

        // en passant not set
        p2.setCanBeEnPassant(false);
        p3.setCanBeEnPassant(false);
        Assert.assertFalse(b.movePiece(p, new Point(0, 5)));
        Assert.assertFalse(b.movePiece(p, new Point(2, 5)));

        // do en passant
        p2.setCanBeEnPassant(true);
        Assert.assertTrue(b.movePiece(p, new Point(0, 5)));
        b.forceMovePiece(p, new Point(1, 4));
        p3.setCanBeEnPassant(true);
        Assert.assertTrue(b.movePiece(p, new Point(2, 5)));
        b.forceMovePiece(p, new Point(1, 4));

        // can only move that way when en passant
        Assert.assertFalse(b.movePiece(p, new Point(0, 5)));
        Assert.assertFalse(b.movePiece(p, new Point(2, 5)));

        // can't enpassant friends
        Pawn p_friend1 = (Pawn)b.getPiece(0, 1);
        Pawn p_friend2 = (Pawn)b.getPiece(2, 1);
        b.forceMovePiece(p_friend1, new Point(0, 4));
        b.forceMovePiece(p_friend2, new Point(2, 4));
        p_friend1.setCanBeEnPassant(true);
        Assert.assertFalse(b.movePiece(p, new Point(0, 5)));
        p_friend2.setCanBeEnPassant(true);
        Assert.assertFalse(b.movePiece(p, new Point(2, 5)));
    }

    @Test
    public void testValidateMove_EnPassantBackward() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Pawn p = (Pawn)b.getPiece(1, 6);
        b.forceMovePiece(p, new Point(1, 3));
        b.forceMovePiece(new Point(0, 1), new Point(0, 3));
        b.forceMovePiece(new Point(2, 1), new Point(2, 3));
        Pawn p2 = (Pawn)b.getPiece(new Point(0, 3));
        Pawn p3 = (Pawn)b.getPiece(new Point(2, 3));

        // en passant not set
        p2.setCanBeEnPassant(false);
        p3.setCanBeEnPassant(false);
        Assert.assertFalse(b.movePiece(p, new Point(0, 2)));
        Assert.assertFalse(b.movePiece(p, new Point(2, 2)));

        // do en passant
        p2.setCanBeEnPassant(true);
        Assert.assertTrue(b.movePiece(p, new Point(0, 2)));
        b.forceMovePiece(p, new Point(1, 3));
        p3.setCanBeEnPassant(true);
        Assert.assertTrue(b.movePiece(p, new Point(2, 2)));
        b.forceMovePiece(p, new Point(1, 3));

        // can only move that way when en passant
        Assert.assertFalse(b.movePiece(p, new Point(0, 2)));
        Assert.assertFalse(b.movePiece(p, new Point(2, 2)));

        // can't enpassant friends
        Pawn p_friend1 = (Pawn)b.getPiece(0, 6);
        Pawn p_friend2 = (Pawn)b.getPiece(2, 6);
        b.forceMovePiece(p_friend1, new Point(0, 3));
        b.forceMovePiece(p_friend2, new Point(2, 3));
        p_friend1.setCanBeEnPassant(true);
        Assert.assertFalse(b.movePiece(p, new Point(0, 2)));
        p_friend2.setCanBeEnPassant(true);
        Assert.assertFalse(b.movePiece(p, new Point(2, 2)));
    }

    @Test
    public void testAttackPath() throws Exception
    {
        Board b = TestsHelper.getEmptyBoard();
        Pawn p = new Pawn(Color.WHITE, new Point(3,3));
        Rook r1 = new Rook(Color.BLACK, new Point(2,2));
        Rook r2 = new Rook(Color.BLACK, new Point(4,2));
        Rook r3 = new Rook(Color.BLACK, new Point(5,4));

        TestsHelper.placeOnBoard(b, p);
        TestsHelper.placeOnBoard(b, r1);
        TestsHelper.placeOnBoard(b, r2);
        TestsHelper.placeOnBoard(b, r3);

        Assert.assertEquals(1, p.getAttackPath(r1, b).size());
        Assert.assertEquals(1, p.getAttackPath(r2, b).size());
        Assert.assertEquals(0, p.getAttackPath(r3, b).size());
    }

    @Test
    public void testOutsideBounds() throws Exception
    {
        Board b = TestsHelper.getSetupBoard();
        Pawn p = (Pawn)b.getPiece(0,1);
        b.movePiece(p, new Point(-1, 3));
    }

    @Test
    public void testSetCanBeEnPassant() throws Exception {
        Pawn p = new Pawn(Color.BLACK, new Point(0,0));
        Assert.assertFalse(p.canBeEnPassant());
        p.setCanBeEnPassant(true);
        Assert.assertTrue(p.canBeEnPassant());
    }

    @Test
    public void testCanBePromoted() throws Exception
    {
        Board b = TestsHelper.getEmptyBoard();
        Pawn pw = new Pawn(Color.WHITE, new Point(0,6));
        Pawn pb = new Pawn(Color.BLACK, new Point(0,1));

        TestsHelper.placeOnBoard(b, pw);
        TestsHelper.placeOnBoard(b, pb);

        Assert.assertFalse(pw.canBePromoted(b));
        Assert.assertFalse(pb.canBePromoted(b));
    }

    @Test
    public void testGetType() throws Exception {
        Pawn p = new Pawn(Color.BLACK, new Point(0,1));
        Assert.assertEquals(p.getType(), PieceType.PAWN);
    }

    @Test
    public void testSetPoint() throws Exception {
        Pawn p = new Pawn(Color.BLACK, new Point(0,1));
        Point po = new Point(1,1);
        p.setPoint(po);
        Assert.assertEquals(p.getPoint(), po);
    }
}