package tests;

import model.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.awt.Point;

/**
 * Created by Tom on 8/14/2015.
 */
public class KingTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testValidateMove_Normal() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        King king = new King(Color.BLACK, new Point(3,3));
        TestsHelper.placeOnBoard(b, king);

        //left, right, up, down
        Assert.assertTrue(b.movePiece(king, new Point(3, 4)));
        Assert.assertTrue(b.movePiece(king, new Point(4, 4)));
        Assert.assertTrue(b.movePiece(king, new Point(4, 3)));
        Assert.assertTrue(b.movePiece(king, new Point(3, 3)));

        Assert.assertFalse(b.movePiece(king, new Point(3, 5)));
        Assert.assertFalse(b.movePiece(king, new Point(5, 3)));
        Assert.assertFalse(b.movePiece(king, new Point(3, 1)));
        Assert.assertFalse(b.movePiece(king, new Point(1, 3)));

        //diagonals
        Assert.assertTrue(b.movePiece(king, new Point(4, 4)));
        Assert.assertTrue(b.movePiece(king, new Point(3, 3)));
        Assert.assertTrue(b.movePiece(king, new Point(2, 4)));
        Assert.assertTrue(b.movePiece(king, new Point(3, 3)));

        Assert.assertFalse(b.movePiece(king, new Point(5, 5)));
        Assert.assertFalse(b.movePiece(king, new Point(1, 1)));
        Assert.assertFalse(b.movePiece(king, new Point(1, 5)));
        Assert.assertFalse(b.movePiece(king, new Point(5, 1)));

        //out of bounds
        Assert.assertFalse(b.movePiece(king, new Point(8, 1)));
        Assert.assertFalse(b.movePiece(king, new Point(4, -1)));
    }

    @Test
    public void testValidateMove_Attacking() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        King king = new King(Color.BLACK, new Point(3,3));
        TestsHelper.placeOnBoard(b, king);
        TestsHelper.placeOnBoard(b, new Pawn(Color.BLACK, new Point(4, 4)));
        TestsHelper.placeOnBoard(b, new Pawn(Color.WHITE, new Point(3, 4)));
        Assert.assertFalse(b.movePiece(king, new Point(4, 4)));
        Assert.assertTrue(b.movePiece(king, new Point(3, 4)));
    }

    @Test
    public void testValidateMove_CastlingLeft() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        b.setBottomColor(Color.BLACK);
        b.clearBoard();
        King king = new King(Color.WHITE, new Point(4,0));
        TestsHelper.placeOnBoard(b, king);
        Rook rook = new Rook(Color.WHITE, new Point(0,0));
        TestsHelper.placeOnBoard(b, rook);
        Pawn pawn1 = new Pawn(Color.WHITE, new Point(1,0));
        TestsHelper.placeOnBoard(b, pawn1);
        Pawn pawn2 = new Pawn(Color.WHITE, new Point(2,0));
        TestsHelper.placeOnBoard(b, pawn2);
        Pawn pawn3 = new Pawn(Color.WHITE, new Point(3,0));
        TestsHelper.placeOnBoard(b, pawn3);

        Assert.assertFalse(b.movePiece(king, new Point(2, 0)));
        b.forceMovePiece(pawn1, new Point(1, 1));
        Assert.assertFalse(b.movePiece(king, new Point(2, 0)));
        b.forceMovePiece(pawn2, new Point(2, 1));
        Assert.assertFalse(b.movePiece(king, new Point(2, 0)));
        b.forceMovePiece(pawn3, new Point(3, 1));
        Assert.assertTrue(b.movePiece(king, new Point(2, 0)));
        Assert.assertNotNull(b.getPiece(3, 0));
    }

    @Test
    public void testValidateMove_CastlingRight() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        b.setBottomColor(Color.BLACK);
        b.clearBoard();
        King king = new King(Color.WHITE, new Point(4,0));
        TestsHelper.placeOnBoard(b, king);
        Rook rook = new Rook(Color.WHITE, new Point(7,0));
        TestsHelper.placeOnBoard(b, rook);
        Pawn pawn1 = new Pawn(Color.WHITE, new Point(5,0));
        TestsHelper.placeOnBoard(b, pawn1);
        Pawn pawn2 = new Pawn(Color.WHITE, new Point(6,0));
        TestsHelper.placeOnBoard(b, pawn2);

        Assert.assertFalse(b.movePiece(king, new Point(6, 0)));
        b.forceMovePiece(pawn1, new Point(5, 1));
        Assert.assertFalse(b.movePiece(king, new Point(6, 0)));
        b.forceMovePiece(pawn2, new Point(6, 1));
        Assert.assertTrue(b.movePiece(king, new Point(6,0)));
        Assert.assertNotNull(b.getPiece(5,0));
    }

    @Test
    public void testCastleThroughCheckLeft() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        King king = new King(Color.WHITE, new Point(4,7));
        Rook r = new Rook(Color.WHITE, new Point(0,7));
        Rook r0 = new Rook(Color.BLACK, new Point(0,3));
        Rook r1 = new Rook(Color.BLACK, new Point(1,3));
        Rook r2 = new Rook(Color.BLACK, new Point(2,3));
        Rook r3 = new Rook(Color.BLACK, new Point(3,3));
        Rook r4 = new Rook(Color.BLACK, new Point(4,3));
        TestsHelper.placeOnBoard(b, king);
        TestsHelper.placeOnBoard(b, r);
        TestsHelper.placeOnBoard(b, r0);
        TestsHelper.placeOnBoard(b, r1);
        TestsHelper.placeOnBoard(b, r2);
        TestsHelper.placeOnBoard(b, r3);
        TestsHelper.placeOnBoard(b, r4);

        Assert.assertFalse(b.movePiece(king, new Point(2, 7)));
        b.forceMovePiece(r4, new Point(6, 6));
        Assert.assertFalse(b.movePiece(king, new Point(2, 7)));
        b.forceMovePiece(r3, new Point(6, 6));
        Assert.assertFalse(b.movePiece(king, new Point(2, 7)));
        b.forceMovePiece(r2, new Point(6, 6));
        Assert.assertTrue(b.movePiece(king, new Point(2, 7)));
    }

    @Test
    public void testCastleThroughCheckRight() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        King king = new King(Color.WHITE, new Point(4,7));
        Rook r = new Rook(Color.WHITE, new Point(7,7));
        Rook r4 = new Rook(Color.BLACK, new Point(4,3));
        Rook r5 = new Rook(Color.BLACK, new Point(5,3));
        Rook r6 = new Rook(Color.BLACK, new Point(6,3));
        Rook r7 = new Rook(Color.BLACK, new Point(7,3));
        TestsHelper.placeOnBoard(b, king);
        TestsHelper.placeOnBoard(b, r);
        TestsHelper.placeOnBoard(b, r4);
        TestsHelper.placeOnBoard(b, r5);
        TestsHelper.placeOnBoard(b, r6);
        TestsHelper.placeOnBoard(b, r7);

        Assert.assertFalse(b.movePiece(king, new Point(6, 7)));
        b.forceMovePiece(r4, new Point(0, 6));
        Assert.assertFalse(b.movePiece(king, new Point(6, 7)));
        b.forceMovePiece(r5, new Point(0, 6));
        Assert.assertFalse(b.movePiece(king, new Point(6, 7)));
        b.forceMovePiece(r6, new Point(0, 6));
        Assert.assertTrue(b.movePiece(king, new Point(6, 7)));
    }

    @Test
    public void testGetAttackPath() throws Exception{
        Board b = TestsHelper.getEmptyBoard();
        King k = new King(Color.WHITE, new Point(4,4));
        Pawn p1 = new Pawn(Color.BLACK, new Point(3,4));
        Pawn p2 = new Pawn(Color.BLACK, new Point(3,5));
        Pawn p3 = new Pawn(Color.BLACK, new Point(4,5));
        Pawn p4 = new Pawn(Color.BLACK, new Point(5,5));
        Pawn p5 = new Pawn(Color.BLACK, new Point(5,4));
        Pawn p6 = new Pawn(Color.BLACK, new Point(5,3));
        Pawn p7 = new Pawn(Color.BLACK, new Point(4,3));
        Pawn p8 = new Pawn(Color.BLACK, new Point(3,3));
        Rook r = new Rook(Color.BLACK, new Point(7,7));

        TestsHelper.placeOnBoard(b, k);
        TestsHelper.placeOnBoard(b, p1);
        TestsHelper.placeOnBoard(b, p2);
        TestsHelper.placeOnBoard(b, p3);
        TestsHelper.placeOnBoard(b, p4);
        TestsHelper.placeOnBoard(b, p5);
        TestsHelper.placeOnBoard(b, p6);
        TestsHelper.placeOnBoard(b, p7);
        TestsHelper.placeOnBoard(b, p8);
        TestsHelper.placeOnBoard(b, r);

        Assert.assertEquals(1, k.getAttackPath(p1, b).size());
        Assert.assertEquals(1, k.getAttackPath(p2, b).size());
        Assert.assertEquals(1, k.getAttackPath(p3, b).size());
        Assert.assertEquals(1, k.getAttackPath(p4, b).size());
        Assert.assertEquals(1, k.getAttackPath(p5, b).size());
        Assert.assertEquals(1, k.getAttackPath(p6, b).size());
        Assert.assertEquals(1, k.getAttackPath(p7, b).size());
        Assert.assertEquals(1, k.getAttackPath(p8, b).size());
        Assert.assertEquals(0, k.getAttackPath(r, b).size());

    }

    @Test
    public void testCanCastle() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        King k = new King(Color.BLACK, new Point(4,0));
        TestsHelper.placeOnBoard(b, k);
        Assert.assertTrue(k.canCastle());
        b.movePiece(k, new Point(3,0));
        Assert.assertFalse(k.canCastle());
    }

    @Test
    public void testGetType() throws Exception {
        King p = new King(Color.BLACK, new Point(0,1));
        Assert.assertEquals(p.getType(), PieceType.KING);
    }
}