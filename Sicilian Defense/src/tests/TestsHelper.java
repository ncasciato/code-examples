package tests;

import model.Board;
import model.Color;
import model.Piece;

import java.lang.reflect.Method;

/**
 * Created by Tom on 8/13/2015.
 */
public class TestsHelper {

    public static Board getSetupBoard()
    {
        Board b = Board.getInstance();
        b.setShowMessages(false);
        b.setBottomColor(Color.WHITE);
        b.setupBoard();
        return b;
    }

    public static Board getEmptyBoard()
    {
        Board b = Board.getInstance();
        b.setShowMessages(false);
        b.setBottomColor(Color.WHITE);
        b.clearBoard();
        return b;
    }

    /*
    We don't want to expose the placePiece method by making it public,
    so we use reflection when we want to place pieces on the board for
    testing purposes
     */
    public static void placeOnBoard(Board b, Piece p) throws Exception
    {
        Method m = b.getClass().getDeclaredMethod("placePiece", Piece.class);
        m.setAccessible(true);
        Object r = m.invoke(b, p);
    }
}
