package tests;

import model.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.awt.Point;
/**
 * Created by sarahcuda on 8/18/15.
 */
public class KnightTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testValidateMoves() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Knight knight = new Knight(Color.BLACK, new Point(2,2));
        TestsHelper.placeOnBoard(b, knight);

        Assert.assertTrue(b.movePiece(knight, new Point(1,4)));
        Assert.assertTrue(b.movePiece(knight, new Point(2,2)));
        Assert.assertTrue(b.movePiece(knight, new Point(0,3)));
        Assert.assertTrue(b.movePiece(knight, new Point(2,2)));
        Assert.assertTrue(b.movePiece(knight, new Point(3,4)));
        Assert.assertTrue(b.movePiece(knight, new Point(2,2)));
        Assert.assertTrue(b.movePiece(knight, new Point(4,3)));
        Assert.assertTrue(b.movePiece(knight, new Point(2,2)));
        Assert.assertTrue(b.movePiece(knight, new Point(0,1)));
        Assert.assertTrue(b.movePiece(knight, new Point(2,2)));
        Assert.assertTrue(b.movePiece(knight, new Point(1,0)));
        Assert.assertTrue(b.movePiece(knight, new Point(2,2)));
        Assert.assertTrue(b.movePiece(knight, new Point(3,0)));
        Assert.assertTrue(b.movePiece(knight, new Point(2,2)));
        Assert.assertTrue(b.movePiece(knight, new Point(4,1)));
        Assert.assertTrue(b.movePiece(knight, new Point(2,2)));

        Assert.assertFalse(b.movePiece(knight, new Point(0,0)));
        Assert.assertFalse(b.movePiece(knight, new Point(4,0)));
        Assert.assertFalse(b.movePiece(knight, new Point(0,4)));
        Assert.assertFalse(b.movePiece(knight, new Point(4,4)));
        Assert.assertFalse(b.movePiece(knight, new Point(2,4)));
        Assert.assertFalse(b.movePiece(knight, new Point(2,0)));
        Assert.assertFalse(b.movePiece(knight, new Point(0,2)));
        Assert.assertFalse(b.movePiece(knight, new Point(4,2)));
    }

    @Test
    public void testAttacks() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Knight knight = new Knight(Color.BLACK, new Point(2,2));
        Rook rook = new Rook(Color.WHITE, new Point(1,4));
        Rook rook2 = new Rook(Color.WHITE, new Point(0,3));
        Bishop bishop = new Bishop(Color.WHITE, new Point(3,4));
        Bishop bishop2 = new Bishop(Color.WHITE, new Point(4,3));
        Pawn pawn = new Pawn(Color.WHITE, new Point(0,1));
        Pawn pawn2 = new Pawn(Color.WHITE, new Point(1,0));
        Pawn pawn3 = new Pawn(Color.WHITE, new Point(3,0));
        Pawn pawn4 = new Pawn(Color.WHITE, new Point(4,1));

        TestsHelper.placeOnBoard(b, knight);
        TestsHelper.placeOnBoard(b, rook);
        TestsHelper.placeOnBoard(b, rook2);
        TestsHelper.placeOnBoard(b, bishop);
        TestsHelper.placeOnBoard(b, bishop2);
        TestsHelper.placeOnBoard(b, pawn);
        TestsHelper.placeOnBoard(b, pawn2);
        TestsHelper.placeOnBoard(b, pawn3);
        TestsHelper.placeOnBoard(b, pawn4);

        Assert.assertTrue(b.movePiece(knight, new Point(1,4)));
        Assert.assertTrue(b.movePiece(knight, new Point(2,2)));
        Assert.assertTrue(b.movePiece(knight, new Point(0,3)));
        Assert.assertTrue(b.movePiece(knight, new Point(2,2)));
        Assert.assertTrue(b.movePiece(knight, new Point(3,4)));
        Assert.assertTrue(b.movePiece(knight, new Point(2,2)));
        Assert.assertTrue(b.movePiece(knight, new Point(4,3)));
        Assert.assertTrue(b.movePiece(knight, new Point(2, 2)));
        Assert.assertTrue(b.movePiece(knight, new Point(0, 1)));
        Assert.assertTrue(b.movePiece(knight, new Point(2, 2)));
        Assert.assertTrue(b.movePiece(knight, new Point(1, 0)));
        Assert.assertTrue(b.movePiece(knight, new Point(2, 2)));
        Assert.assertTrue(b.movePiece(knight, new Point(3, 0)));
        Assert.assertTrue(b.movePiece(knight, new Point(2, 2)));
        Assert.assertTrue(b.movePiece(knight, new Point(4, 1)));
    }

    @Test
    public void testMoveThrough() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Knight knight = new Knight(Color.BLACK, new Point(3,1));
        Rook rook = new Rook(Color.BLACK, new Point(4,3));

        TestsHelper.placeOnBoard(b, knight);
        TestsHelper.placeOnBoard(b, rook);

        Assert.assertFalse(b.movePiece(knight, new Point(4,3)));
    }

    @Test
    public void testGetAttackPath() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Knight k = new Knight(Color.WHITE, new Point(3,4));
        Pawn p1 = new Pawn(Color.BLACK, new Point(1,5));
        Pawn p2 = new Pawn(Color.BLACK, new Point(1,3));
        Pawn p3 = new Pawn(Color.BLACK, new Point(4,6));
        Pawn p4 = new Pawn(Color.BLACK, new Point(2,6));
        Pawn p5 = new Pawn(Color.BLACK, new Point(5,5));
        Pawn p6 = new Pawn(Color.BLACK, new Point(5,3));
        Pawn p7 = new Pawn(Color.BLACK, new Point(4,2));
        Pawn p8 = new Pawn(Color.BLACK, new Point(2,2));
        Rook r = new Rook(Color.BLACK, new Point(7,7));

        TestsHelper.placeOnBoard(b, k);
        TestsHelper.placeOnBoard(b, p1);
        TestsHelper.placeOnBoard(b, p2);
        TestsHelper.placeOnBoard(b, p3);
        TestsHelper.placeOnBoard(b, p4);
        TestsHelper.placeOnBoard(b, p5);
        TestsHelper.placeOnBoard(b, p6);
        TestsHelper.placeOnBoard(b, p7);
        TestsHelper.placeOnBoard(b, p8);
        TestsHelper.placeOnBoard(b, r);

        Assert.assertEquals(1, k.getAttackPath(p1, b).size());
        Assert.assertEquals(1, k.getAttackPath(p2, b).size());
        Assert.assertEquals(1, k.getAttackPath(p3, b).size());
        Assert.assertEquals(1, k.getAttackPath(p4, b).size());
        Assert.assertEquals(1, k.getAttackPath(p5, b).size());
        Assert.assertEquals(1, k.getAttackPath(p6, b).size());
        Assert.assertEquals(1, k.getAttackPath(p7, b).size());
        Assert.assertEquals(1, k.getAttackPath(p8, b).size());
        Assert.assertEquals(0, k.getAttackPath(r, b).size());
    }

    @Test
    public void testOutOfBounds() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Knight knight = new Knight(Color.BLACK, new Point(1,1));
        TestsHelper.placeOnBoard(b, knight);

        Assert.assertFalse(b.movePiece(knight, new Point(-1,0)));
        Assert.assertFalse(b.movePiece(knight, new Point(-1,2)));
        Assert.assertFalse(b.movePiece(knight, new Point(0,-1)));
        Assert.assertFalse(b.movePiece(knight, new Point(2,-1)));
    }

    @Test
    public void testGetType() throws Exception {
        Knight k = new Knight(Color.BLACK, new Point(2,2));
        Assert.assertEquals(k.getType(), PieceType.KNIGHT);
    }

}
