package tests;

import model.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.awt.Point;
import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by Tom on 8/13/2015.
 */
public class RookTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testValidateMove_Right() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Rook r = (Rook)b.getPiece(0,0);
        b.forceMovePiece(new Point(1,6), new Point(3,4));
        b.forceMovePiece(new Point(2,6), new Point(4,4));
        b.forceMovePiece(r, new Point(0,4));
        Assert.assertFalse(b.movePiece(r, new Point(5, 4)));
        Assert.assertFalse(b.movePiece(r, new Point(4, 4)));
        Assert.assertTrue(b.movePiece(r, new Point(1, 4)));
        Assert.assertTrue(b.movePiece(r, new Point(3, 4)));
        Assert.assertFalse(b.movePiece(r, new Point(5, 4)));
        Assert.assertTrue(b.movePiece(r, new Point(4, 4)));
        Assert.assertFalse(b.movePiece(r, new Point(8, 4)));
        b.forceMovePiece(new Point(1, 1), new Point(7, 4));
        Assert.assertFalse(b.movePiece(r, new Point(7, 4)));
        Assert.assertFalse(b.movePiece(r, new Point(7, 5)));
    }

    @Test
    public void testValidateMove_Left() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Rook r = (Rook)b.getPiece(7,0);
        b.forceMovePiece(new Point(1,6), new Point(3,4));
        b.forceMovePiece(new Point(2,6), new Point(4,4));
        b.forceMovePiece(r, new Point(7,4));
        Assert.assertFalse(b.movePiece(r, new Point(2, 4)));
        Assert.assertFalse(b.movePiece(r, new Point(3, 4)));
        Assert.assertTrue(b.movePiece(r, new Point(6, 4)));
        Assert.assertTrue(b.movePiece(r, new Point(4, 4)));
        Assert.assertFalse(b.movePiece(r, new Point(2, 4)));
        Assert.assertTrue(b.movePiece(r, new Point(3, 4)));
        Assert.assertFalse(b.movePiece(r, new Point(-1, 4)));
        b.forceMovePiece(new Point(1, 1), new Point(0, 4));
        Assert.assertFalse(b.movePiece(r, new Point(0, 4)));
        Assert.assertFalse(b.movePiece(r, new Point(0, 5)));
    }

    @Test
    public void testValidateMove_Down() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Rook r = (Rook)b.getPiece(0,0);
        b.forceMovePiece(r, new Point(0,7));
        b.forceMovePiece(new Point(1,6), new Point(0,3));
        b.forceMovePiece(new Point(2,6), new Point(0,4));
        b.forceMovePiece(new Point(0,1), new Point(5,4));
        b.forceMovePiece(new Point(0,6), new Point(5,4));
        b.forceMovePiece(new Point(1,0), new Point(5,4));
        Assert.assertFalse(b.movePiece(r, new Point(0, 2)));
        Assert.assertFalse(b.movePiece(r, new Point(0, 3)));
        Assert.assertTrue(b.movePiece(r, new Point(0, 6)));
        Assert.assertTrue(b.movePiece(r, new Point(0, 4)));
        Assert.assertFalse(b.movePiece(r, new Point(0, 2)));
        Assert.assertTrue(b.movePiece(r, new Point(0, 3)));
        Assert.assertFalse(b.movePiece(r, new Point(0, -1)));
        b.forceMovePiece(new Point(1, 1), new Point(0, 0));
        Assert.assertFalse(b.movePiece(r, new Point(0, 0)));
        Assert.assertFalse(b.movePiece(r, new Point(1, 0)));
    }

    @Test
    public void testValidateMove_Up() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Rook r = (Rook)b.getPiece(0,0);
        b.forceMovePiece(new Point(1, 6), new Point(0, 3));
        b.forceMovePiece(new Point(2, 6), new Point(0, 4));
        b.forceMovePiece(new Point(0, 1), new Point(5, 4));
        b.forceMovePiece(new Point(0, 6), new Point(5, 4));
        b.forceMovePiece(new Point(0, 7), new Point(5, 4));
        b.forceMovePiece(new Point(1, 7), new Point(5, 4));
        Assert.assertFalse(b.movePiece(r, new Point(0, 5)));
        Assert.assertFalse(b.movePiece(r, new Point(0, 4)));
        Assert.assertTrue(b.movePiece(r, new Point(0, 1)));
        Assert.assertTrue(b.movePiece(r, new Point(0, 3)));
        Assert.assertFalse(b.movePiece(r, new Point(0, 5)));
        Assert.assertTrue(b.movePiece(r, new Point(0, 4)));
        Assert.assertFalse(b.movePiece(r, new Point(0, 8)));
        b.forceMovePiece(new Point(1, 1), new Point(0, 7));
        Assert.assertFalse(b.movePiece(r, new Point(0, 7)));
        Assert.assertFalse(b.movePiece(r, new Point(1, 7)));
    }

    @Test
    public void testGetAttackPath() throws Exception {
        Board b = TestsHelper.getEmptyBoard();
        Rook r = new Rook(Color.BLACK, new Point(3, 3));
        Knight k1 = new Knight(Color.WHITE, new Point(2, 3));
        Knight k2 = new Knight(Color.WHITE, new Point(3, 1));
        Knight k3 = new Knight(Color.WHITE, new Point(6, 3));
        Knight k4 = new Knight(Color.WHITE, new Point(3, 7));
        Knight k5 = new Knight(Color.WHITE, new Point(3, 3));

        TestsHelper.placeOnBoard(b, r);
        TestsHelper.placeOnBoard(b, k1);
        TestsHelper.placeOnBoard(b, k2);
        TestsHelper.placeOnBoard(b, k3);
        TestsHelper.placeOnBoard(b, k4);
        TestsHelper.placeOnBoard(b, k5);

        ArrayList<Point> path1 = r.getAttackPath(k1, b);
        ArrayList<Point> path2 = r.getAttackPath(k2, b);
        ArrayList<Point> path3 = r.getAttackPath(k3, b);
        ArrayList<Point> path4 = r.getAttackPath(k4, b);
        ArrayList<Point> path5 = r.getAttackPath(k5, b);

        Assert.assertEquals(1, path1.size());
        Assert.assertEquals(2, path2.size());
        Assert.assertEquals(3, path3.size());
        Assert.assertEquals(4, path4.size());
        Assert.assertEquals(0, path5.size());
    }

    @Test
    public void testCanCastle() throws Exception
    {
        Board b = TestsHelper.getEmptyBoard();
        Rook r = new Rook(Color.BLACK, new Point(0,0));
        TestsHelper.placeOnBoard(b, r);
        Assert.assertTrue(r.canCastle());
        b.movePiece(r, new Point(1,0));
        Assert.assertFalse(r.canCastle());
    }

    @Test
    public void testGetType() throws Exception {
        Rook r = new Rook(Color.BLACK, new Point(0, 1));
        Assert.assertEquals(r.getType(), PieceType.ROOK);
    }
}