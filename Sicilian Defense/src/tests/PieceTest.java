package tests;

import model.Color;
import model.Piece;
import model.Queen;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import java.awt.*;

/**
 * Created by Tom on 7/27/2015.
 */
public class PieceTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetColor() throws Exception
    {
        Piece p = new Queen(Color.WHITE, new Point(0,0));
        Assert.assertEquals(Color.WHITE, p.getColor());
    }

    @Test
    public void testGetPoint() throws Exception
    {
        Piece p = new Queen(Color.WHITE, new Point(1,3));
        Assert.assertEquals(p.getPoint().getX(), 1.0, 0);
        Assert.assertEquals(p.getPoint().getY(), 3.0, 0);
    }

    @Test
    public void testSetPoint() throws Exception
    {
        Piece p = new Queen(Color.WHITE, new Point(1,3));
        p.setPoint(new Point(2, 4));
        Assert.assertEquals(p.getPoint().getX(), 2.0, 0);
        Assert.assertEquals(p.getPoint().getY(), 4.0, 0);

        boolean wasThrown = false;
        try {
            p.setPoint(new Point(-1,0));
        }
        catch (IndexOutOfBoundsException ex){
            wasThrown = true;
        }
        if(!wasThrown)
           throw new Exception();

        wasThrown = false;
        try {
            p.setPoint(new Point(1,-1));
        }
        catch (IndexOutOfBoundsException ex){
            wasThrown = true;
        }
        if(!wasThrown)
            throw new Exception();

        wasThrown = false;
        try {
            p.setPoint(new Point(10,1));
        }
        catch (IndexOutOfBoundsException ex){
            wasThrown = true;
        }
        if(!wasThrown)
            throw new Exception();

        wasThrown = false;
        try {
            p.setPoint(new Point(1,10));
        }
        catch (IndexOutOfBoundsException ex){
            wasThrown = true;
        }
        if(!wasThrown)
            throw new Exception();

    }
}