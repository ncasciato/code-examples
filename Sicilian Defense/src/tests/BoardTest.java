package tests;

import org.junit.Assert;
import model.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.awt.Point;
import java.util.ArrayList;

/**
 * Created by Tom on 7/27/2015.
 */
public class BoardTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testGetInstance() throws Exception {
        Board b = Board.getInstance();
        Board b2 = Board.getInstance();
        Assert.assertEquals(b, b2);
    }

    @Test
    public void testMovePiece() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        Assert.assertTrue(b.movePiece(new Point(0,1), new Point(0,2)));
        Assert.assertNotNull(b.getPiece(new Point(0, 2)));

        Piece p = b.getPiece(new Point(1,1));
        Assert.assertNotNull(b);
        Assert.assertTrue(b.movePiece(p, new Point(1, 2)));
        Assert.assertNotNull(b.getPiece(1, 2));

        Assert.assertFalse(b.movePiece(new Point(0, 1), new Point(0, 2)));
        Assert.assertFalse(b.movePiece(p, new Point(1, 1)));
    }

    @Test
    public void testForceMovePiece() throws Exception {
        Board b = TestsHelper.getSetupBoard();
        b.forceMovePiece(new Point(0, 1), new Point(0, 4));
        Assert.assertNotNull(b.getPiece(0, 4));
        b.forceMovePiece(new Point(1, 3), new Point(0, 1));
    }

    @Test
    public void testCountMoves() throws Exception {
        Rook r = new Rook(Color.BLACK, new Point(0,0));
        Board board = TestsHelper.getEmptyBoard();
        TestsHelper.placeOnBoard(board, r);
        Assert.assertFalse(board.fiftyMoveRule());
        for(int i = 0; i < 50; i++) {
            board.forceMovePiece(new Point(0,0), new Point(0, 4));
            board.movePiece(r, new Point(0, 0));
        }
        Assert.assertTrue(board.fiftyMoveRule());

        Assert.assertFalse(board.seventyFiveMoveRule());
        for(int i = 0; i < 25; i++) {
            board.forceMovePiece(new Point(0,0), new Point(0, 4));
            board.movePiece(r, new Point(0, 0));
        }
        Assert.assertTrue(board.seventyFiveMoveRule());
    }

    @Test
    public void testInCheck() throws Exception {
        King k = new King(Color.WHITE, new Point(4,7));
        Knight n = new Knight(Color.BLACK, new Point(3,5));
        Rook r = new Rook(Color.BLACK, new Point(4,0));
        Bishop b = new Bishop(Color.BLACK, new Point(5,6));
        Pawn p = new Pawn(Color.BLACK, new Point(3,6));
        Queen q = new Queen(Color.BLACK, new Point(7,7));

        Board board = TestsHelper.getEmptyBoard();
        TestsHelper.placeOnBoard(board, k);
        TestsHelper.placeOnBoard(board, n);
        Assert.assertTrue(board.inCheck(Color.WHITE));

        board = TestsHelper.getEmptyBoard();
        TestsHelper.placeOnBoard(board, k);
        TestsHelper.placeOnBoard(board, r);
        Assert.assertTrue(board.inCheck(Color.WHITE));

        board = TestsHelper.getEmptyBoard();
        TestsHelper.placeOnBoard(board, k);
        TestsHelper.placeOnBoard(board, b);
        Assert.assertTrue(board.inCheck(Color.WHITE));

        board = TestsHelper.getEmptyBoard();
        TestsHelper.placeOnBoard(board, k);
        TestsHelper.placeOnBoard(board, p);
        Assert.assertTrue(board.inCheck(Color.WHITE));

        board = TestsHelper.getEmptyBoard();
        TestsHelper.placeOnBoard(board, k);
        TestsHelper.placeOnBoard(board, q);
        Assert.assertTrue(board.inCheck(Color.WHITE));
    }

    @Test
    public void testCannotMoveIntoCheck() throws Exception
    {
        Board board = TestsHelper.getEmptyBoard();
        King k = new King(Color.WHITE, new Point(4,0));
        Knight n = new Knight(Color.WHITE, new Point(1,0));
        Rook r = new Rook(Color.BLACK, new Point(0,0));
        Rook r2 = new Rook(Color.BLACK, new Point(0,1));
        Pawn p = new Pawn(Color.BLACK, new Point(4,1));
        TestsHelper.placeOnBoard(board, k);
        TestsHelper.placeOnBoard(board, n);
        TestsHelper.placeOnBoard(board, r);
        TestsHelper.placeOnBoard(board, r2);
        TestsHelper.placeOnBoard(board, p);

        Assert.assertFalse(board.movePiece(n, new Point(2, 2)));
        Assert.assertFalse(board.movePiece(k, new Point(4, 1)));
        Assert.assertNotNull(board.getPiece(4, 1));

        board = TestsHelper.getEmptyBoard();
        k = new King(Color.BLACK, new Point(4,7));
        n = new Knight(Color.BLACK, new Point(1,7));
        r = new Rook(Color.WHITE, new Point(0,7));
        r2 = new Rook(Color.WHITE, new Point(0,6));
        p = new Pawn(Color.WHITE, new Point(4,6));
        TestsHelper.placeOnBoard(board, k);
        TestsHelper.placeOnBoard(board, n);
        TestsHelper.placeOnBoard(board, r);
        TestsHelper.placeOnBoard(board, r2);
        TestsHelper.placeOnBoard(board, p);

        Assert.assertFalse(board.movePiece(n, new Point(2, 5)));
        Assert.assertFalse(board.movePiece(k, new Point(4, 6)));
        Assert.assertNotNull(board.getPiece(4, 6));

        board = TestsHelper.getEmptyBoard();
        TestsHelper.placeOnBoard(board, k);
        TestsHelper.placeOnBoard(board, r);
        Assert.assertFalse(board.movePiece(k, new Point(3,7)));
    }

    @Test
    public void testCannotEnPassantIntoCheck() throws Exception
    {
        Board board = TestsHelper.getEmptyBoard();
        King k = new King(Color.WHITE, new Point(4,0));
        Queen q = new Queen(Color.BLACK, new Point(4,7));
        Pawn pw = new Pawn(Color.WHITE, new Point(4,4));
        Pawn pb = new Pawn(Color.BLACK, new Point(3,4));
        pb.setCanBeEnPassant(true);
        TestsHelper.placeOnBoard(board, k);
        TestsHelper.placeOnBoard(board, q);
        TestsHelper.placeOnBoard(board, pw);
        TestsHelper.placeOnBoard(board, pb);
        Assert.assertFalse(board.movePiece(pw, new Point(3, 3)));
        Assert.assertNotNull(board.getPiece(new Point(3, 4)));
        Assert.assertNotNull(board.getPiece(new Point(4, 4)));
    }

    @Test
    public void testInCheckMate() throws Exception
    {
        Board board = TestsHelper.getEmptyBoard();
        King k = new King(Color.WHITE, new Point(4,0));
        Rook r1 = new Rook(Color.BLACK, new Point(3,4));
        Rook r2 = new Rook(Color.BLACK, new Point(5,4));
        Bishop b = new Bishop(Color.BLACK, new Point(7,3));
        Queen q = new Queen(Color.BLACK, new Point(4,3));
        Pawn p = new Pawn(Color.WHITE, new Point(4,2));
        Rook r = new Rook(Color.WHITE, new Point(0,2));
        TestsHelper.placeOnBoard(board, k);
        Assert.assertFalse(board.inCheckmate(Color.WHITE));
        TestsHelper.placeOnBoard(board, r1);
        Assert.assertFalse(board.inCheckmate(Color.WHITE));
        TestsHelper.placeOnBoard(board, r2);
        Assert.assertFalse(board.inCheckmate(Color.WHITE));
        TestsHelper.placeOnBoard(board, b);
        Assert.assertFalse(board.inCheckmate(Color.WHITE));
        TestsHelper.placeOnBoard(board, q);
        Assert.assertTrue(board.inCheckmate(Color.WHITE));
        TestsHelper.placeOnBoard(board, p);
        Assert.assertFalse(board.inCheckmate(Color.WHITE));
        board.forceMovePiece(q, p.getPoint());
        Assert.assertTrue(board.inCheckmate(Color.WHITE));
        TestsHelper.placeOnBoard(board, r);
        Assert.assertTrue(board.inCheckmate(Color.WHITE));
        board.forceMovePiece(b, new Point(7, 7));
        Assert.assertFalse(board.inCheckmate(Color.WHITE));
        board.forceMovePiece(r, new Point(0, 7));
        Assert.assertTrue(board.inCheckmate(Color.WHITE));
    }

    @Test
    public void testCapturedPieces() throws Exception
    {
        Board board = TestsHelper.getEmptyBoard();
        Rook r = new Rook(Color.WHITE, new Point(0,0));
        Pawn p1 = new Pawn(Color.BLACK, new Point(0,4));
        Pawn p2 = new Pawn(Color.WHITE, new Point(2,2));
        Knight n = new Knight(Color.BLACK, new Point(4,4));
        Bishop b = new Bishop(Color.BLACK, new Point(6,6));
        TestsHelper.placeOnBoard(board, r);
        TestsHelper.placeOnBoard(board, p1);
        TestsHelper.placeOnBoard(board, p2);
        TestsHelper.placeOnBoard(board, n);
        TestsHelper.placeOnBoard(board, b);

        board.movePiece(r, p1.getPoint());
        board.movePiece(r, n.getPoint());
        ArrayList<Piece> blacks = board.getCapturedBlackPieces();
        Assert.assertEquals(2, blacks.size());
        Assert.assertTrue(blacks.contains(p1));
        Assert.assertTrue(blacks.contains(n));

        board.movePiece(b, r.getPoint());
        board.movePiece(b, p2.getPoint());
        ArrayList<Piece> whites = board.getCapturedWhitePieces();
        Assert.assertEquals(2, whites.size());
        Assert.assertTrue(whites.contains(r));
        Assert.assertTrue(whites.contains(p2));
    }

    @Test
    public void testPromotion() throws Exception
    {
        Board b = TestsHelper.getEmptyBoard();
        Pawn pw = new Pawn(Color.WHITE, new Point(0,6));
        Pawn pb = new Pawn(Color.BLACK, new Point(0,1));
        Queen q = new Queen(Color.WHITE, new Point(3,7));
        Pawn pw2 = new Pawn(Color.WHITE, new Point(1,0));
        Pawn pb2 = new Pawn(Color.BLACK, new Point(1,7));

        TestsHelper.placeOnBoard(b, pw);
        TestsHelper.placeOnBoard(b, pb);
        TestsHelper.placeOnBoard(b, pw2);
        TestsHelper.placeOnBoard(b, pb2);
        TestsHelper.placeOnBoard(b, q);

        Assert.assertFalse(b.validPromotion(q));
        b.promote(q, PieceType.KNIGHT);
        Assert.assertEquals(q, b.getPiece(q.getPoint()));

        Assert.assertFalse(b.validPromotion(pw));
        Assert.assertFalse(b.validPromotion(pb));
        b.forceMovePiece(pw, new Point(0, 0));
        b.forceMovePiece(pb, new Point(0, 7));

        Assert.assertTrue(b.validPromotion(pw));
        Assert.assertTrue(b.validPromotion(pb));
        Assert.assertTrue(b.validPromotion(pw2));
        Assert.assertTrue(b.validPromotion(pb2));

        b.promote(pw, PieceType.KING);
        b.promote(pb, PieceType.PAWN);
        Assert.assertEquals(pw, b.getPiece(pw.getPoint()));
        Assert.assertEquals(pb, b.getPiece(pb.getPoint()));

        b.promote(pw, PieceType.QUEEN);
        b.promote(pb, PieceType.BISHOP);
        b.promote(pw2, PieceType.ROOK);
        b.promote(pb2, PieceType.KNIGHT);
        b.getHistory();
        Assert.assertEquals(PieceType.QUEEN, b.getPiece(pw.getPoint()).getType());
        Assert.assertEquals(PieceType.BISHOP, b.getPiece(pb.getPoint()).getType());
        Assert.assertEquals(PieceType.ROOK, b.getPiece(pw2.getPoint()).getType());
        Assert.assertEquals(PieceType.KNIGHT, b.getPiece(pb2.getPoint()).getType());
    }

    @Test
    public void testSetBottomColor() throws Exception
    {
        Board b = TestsHelper.getEmptyBoard();
        Assert.assertEquals(Color.WHITE, b.getBottomColor());
        b.setBottomColor(Color.BLACK);
        Assert.assertEquals(Color.BLACK, b.getBottomColor());
        b.setupBoard();
        Piece king = b.getPiece(new Point(3,7));
        Assert.assertTrue(king instanceof King);
        Assert.assertTrue(king.getColor() == Color.BLACK);
    }

    @Test
    public void testStalemate() throws Exception
    {
        Board b = TestsHelper.getEmptyBoard();
        King bk = new King(Color.BLACK, new Point(6,0));
        Queen q = new Queen(Color.WHITE, new Point(7, 2));
        King wk = new King(Color.WHITE, new Point(5, 2));
        TestsHelper.placeOnBoard(b, bk);
        TestsHelper.placeOnBoard(b, q);
        TestsHelper.placeOnBoard(b, wk);
        Assert.assertTrue(b.isInStalemate(Color.BLACK));

        b.movePiece(q, new Point(7, 3));
        Assert.assertFalse(b.isInStalemate(Color.BLACK));

    }

    @Test
    public void testImpossibleCheck() throws Exception
    {
        Board b = TestsHelper.getEmptyBoard();
        King bk = new King(Color.BLACK, new Point(3,0));
        King wk = new King(Color.WHITE, new Point(3,7));
        TestsHelper.placeOnBoard(b, bk);
        TestsHelper.placeOnBoard(b, wk);
        Assert.assertFalse(b.isCheckmatePossible());

        Knight wn = new Knight(Color.WHITE, new Point(5,0));
        TestsHelper.placeOnBoard(b, wn);
        Assert.assertFalse(b.isCheckmatePossible());

        Bishop wb = new Bishop(Color.WHITE, new Point(4,1));
        TestsHelper.placeOnBoard(b, wb);
        Assert.assertFalse(b.isCheckmatePossible());

        Bishop bb = new Bishop(Color.BLACK, new Point(4,7));
        TestsHelper.placeOnBoard(b, bb);
        Assert.assertFalse(b.isCheckmatePossible());

        Knight bn = new Knight(Color.BLACK, new Point(5,7));
        TestsHelper.placeOnBoard(b, bn);
        Assert.assertTrue(b.isCheckmatePossible());

        Bishop wb2 = new Bishop(Color.WHITE, new Point(2, 2));
        TestsHelper.placeOnBoard(b, wb2);
        Assert.assertTrue(b.isCheckmatePossible());

        Bishop bb2 = new Bishop(Color.BLACK, new Point(2, 6));
        TestsHelper.placeOnBoard(b, bb2);
        Assert.assertTrue(b.isCheckmatePossible());

        b = TestsHelper.getEmptyBoard();
        Queen wq = new Queen(Color.WHITE, new Point(2, 1));
        TestsHelper.placeOnBoard(b, wq);
        Assert.assertTrue(b.isCheckmatePossible());

        b = TestsHelper.getEmptyBoard();
        Rook wr = new Rook(Color.WHITE, new Point(2, 1));
        TestsHelper.placeOnBoard(b, wr);
        Assert.assertTrue(b.isCheckmatePossible());

        b = TestsHelper.getEmptyBoard();
        Pawn wp = new Pawn(Color.WHITE, new Point(2, 1));
        TestsHelper.placeOnBoard(b, wp);
        Assert.assertTrue(b.isCheckmatePossible());

        b = TestsHelper.getEmptyBoard();
        TestsHelper.placeOnBoard(b, wb);
        TestsHelper.placeOnBoard(b, bb);
        TestsHelper.placeOnBoard(b, bb2);
        Assert.assertTrue(b.isCheckmatePossible());

        b = TestsHelper.getEmptyBoard();
        TestsHelper.placeOnBoard(b, wb);
        TestsHelper.placeOnBoard(b, bb);
        TestsHelper.placeOnBoard(b, bb2);
        Assert.assertTrue(b.isCheckmatePossible());

        b = TestsHelper.getEmptyBoard();
        TestsHelper.placeOnBoard(b, wb);
        TestsHelper.placeOnBoard(b, bb2);
        Assert.assertTrue(b.isCheckmatePossible());

        b = TestsHelper.getEmptyBoard();
        TestsHelper.placeOnBoard(b, wb2);
        TestsHelper.placeOnBoard(b, bb2);
        Assert.assertFalse(b.isCheckmatePossible());
    }

    @Test
    public void testDoubleMoveCausesCheck() throws Exception
    {
        Board board = TestsHelper.getEmptyBoard();
        Pawn p = new Pawn(Color.BLACK, new Point(2,1));
        King k = new King(Color.BLACK, new Point(1,0));
        Bishop b = new Bishop(Color.WHITE, new Point(3,2));

        TestsHelper.placeOnBoard(board, p);
        TestsHelper.placeOnBoard(board, k);
        TestsHelper.placeOnBoard(board, b);

        Assert.assertFalse(board.inCheck(Color.BLACK));
        Assert.assertFalse(board.movePiece(p, new Point(2, 3)));
    }

    @Test
    public void forceMoveSpecialCases() throws Exception
    {
        Board board = TestsHelper.getEmptyBoard();
        King k = new King(Color.WHITE, new Point(3,7));
        Rook r = new Rook(Color.WHITE, new Point(0,7));
        TestsHelper.placeOnBoard(board, k);
        TestsHelper.placeOnBoard(board, r);

        board.forceMovePiece(k, new Point(1, 7));
        Assert.assertNull(board.getPiece(3, 7));
        Assert.assertNull(board.getPiece(0,7));

        Pawn p = new Pawn(Color.WHITE, new Point(3,4));
        Pawn p2 = new Pawn(Color.BLACK, new Point(4,4));
        p2.setCanBeEnPassant(true);
        TestsHelper.placeOnBoard(board, p);
        TestsHelper.placeOnBoard(board, p2);
        board.forceMovePiece(p, new Point(4,3));
        Assert.assertNull(board.getPiece(3, 4));
        Assert.assertNull(board.getPiece(4,4));
    }
}