// Nichole Casciato
// February 10, 2017
// nicholecasciato@gmail.com
//
// This is a concurrent mergesort program which generates a random array and
//	runs the mergesort algorithm until the slices are beneath the sort threshold
//	at which time is sorted with a selection sort algorithm

package main

import "fmt"
import "math/rand"
import "os"
import "strconv"
import "time"

var minArray int = 50;
var maxArray int = 500;
var sort_threshold int = 20;

func main() {
	size, min, max := parse_args();

	fmt.Printf( "\nMin: %v\nMax: %v\n", min, max );
	numbers := random_array( size, min, max );

	if ( size == 0 ) {
		size = len( numbers );
	}

	fmt.Printf( "\nRandom Array:\n%v\n\n", numbers );

	number_chan := make( chan []int, 1 );
	number_chan <- numbers;
	sorted_chan := make( chan int, size );
	sorted := make( []int, size );

	go merge_sort( number_chan, sorted_chan );

	for i := 0; i < size; i++ {
		sorted[i] = <-sorted_chan;
	}

	fmt.Printf( "Sorted Array:\n%v\n\n", sorted );
}

// parse_args parses the command line and extracts any program parameters
//	the user specifies. Otherwise, it returns predefined values for each
//	argument the program expects.
//
// This function takes no parameters
//
// return:
//	- size: size of the random array. Default is 0 (array size to be randomly generated)
//	- min:	minimum number to be randomly generated. Default is 0.
//	- max:	maximum number to be randomly generated. Default is 1,000,000.
func parse_args() ( int, int, int ) {
	var size int = 0;
	var min int = 0;
	var max int = 1000000;

	for i := 1; i < len( os.Args ); i++ {
		var err error;

		if ( os.Args[i] == "-size" ) {
			i = i + 1;
			size, err = strconv.Atoi( os.Args[i] );
		} else if ( os.Args[i] == "-min" ) {
			i = i + 1;
			min, err = strconv.Atoi( os.Args[i] );
		} else if ( os.Args[i] == "-max" ) {
			i = i + 1;
			max, err = strconv.Atoi( os.Args[i] );
		} else if ( os.Args[i] == "-threshold" ) {
			i = i + 1;
			sort_threshold, err = strconv.Atoi( os.Args[i] );
		} else if ( os.Args[i] == "-help" ) {
			fmt.Print( "\nWelcome to mergesort in Go!\n" )
			fmt.Print( "This program mergesorts an array or randomly generated numbers between a minimum and maximum.\n" );
			fmt.Print( "\t-min [value] - minimum value to be generated in the array [default = 0]\n" );
			fmt.Print( "\t-max [value] - maximum value to be generated in the array [default = 1,000,000]\n" );
			fmt.Print( "\t-size [value] - size of the array to be generated [default is an array between 50 and 500 values]\n" );
			fmt.Print( "\t-threshold [value] - the sort threshold below which the array is just sorted by the select sort algorithm [default = 20]\n\n" );
			os.Exit( 0 );
		} else {
			fmt.Print( "\n UNEXPECTED PARAMETER. EXITING.\n" );
			os.Exit( 1 );
		}

		if ( err != nil ) {
			fmt.Printf( "%v\n", err );
		}
	}

	if ( min > max ) {
		tmp := min;
		min = max;
		max = tmp;
	}

	return size, min, max;
}

// random_array creates an array of randomly generated numbers between a minimum
//	and maximum of a specified array size
//
// parameters:
//	- size: size of the array to make. 0 mena sthe number of items should be a
//			randomly generated number
//	- min:	minimum value to be generated
//	- max:	maximum value to be generated
//
// return:
//	- array of randomly generated values
func random_array( size int, min int, max int ) []int {
	s1 := rand.NewSource( time.Now().UnixNano() );
	rnd := rand.New( s1 );

	if ( size == 0 ) {
		size = rnd.Intn( maxArray - minArray ) + minArray;
	}

	numbers := make( []int, size );
	for i := 0; i < size; i++ {
		numbers[i] = rnd.Intn( max - min ) + min;
	}

	return numbers;
}

// This is the recursive function which divides the array in half until
//	the whole array is divided into arrays smaller than the sort threshold
// 
// parameters:
//	- number_chan:	a channel containing the numbers to be sorted
//	- out_chan:		a channel containing sorted numbers
//
// no explicit return
func merge_sort( number_chan chan []int, out_chan chan int ) {
	numbers := <- number_chan;

	if ( len( numbers ) <= sort_threshold ) {
		number_chan <- numbers;
		selection_sort( number_chan, out_chan );
		return;
	}

	var left, right chan []int;
	var left_out, right_out chan int;

	if ( len( numbers ) % 2 == 0 ) { // EVEN
		half := len( numbers ) / 2;

		l := numbers[:half];
		left = make( chan []int, 1 );
		left_out = make( chan int, len( l ));
		left <- l;

		r := numbers[half:];
		right = make( chan []int, 1 );
		right_out = make( chan int, len( r ));
		right <- r;
	} else { // ODD
		half := len( numbers ) / 2;

		l := numbers[:half];
		left = make( chan []int, 1 );
		left_out = make( chan int, len( l ));
		left <- l;

		r := numbers[half:];
		right = make( chan []int, 1 );
		right_out = make( chan int, len( r ));
		right <- r;
	}

	go merge_sort( left, left_out );
	go merge_sort( right, right_out );

	merge( left_out, right_out, out_chan );
}

// This function merges two channels of sorted numbers into a single channel
//
// parameters:
//	- left:		a channel containing sorted numbers to be merged
//	- right:	a channel containing sorted numbers to be merged
//	- out_chan:	a channel containing merged sorted numbers
//
// no explicit return
func merge( left chan int, right chan int, out_chan chan int ) {
	length := cap( left ) + cap( right );

	l := -1;
	r := -1;
	l_index := 0;
	r_index := 0;

	for i := 0; i < length; i++ {
		if ( l < 0 && l_index < cap( left )) {
			l = <- left;
			l_index++;
		}
		if ( r < 0 && r_index < cap( right )) {
			r = <- right;
			r_index++;
		}

		if ( l >= 0 && r >= 0 ) {
			if ( l < r ) {
				out_chan <- l;
				l = -1;
			} else { // l > r
				out_chan <- r;
				r = -1;
			}
		} else if ( l >= 0 ) {
			out_chan <- l;
			l = -1;
		} else { // r >= 0
			out_chan <- r;
			r = -1;
		}
	}
}

// This function is a selection sort for slices of the orginal array which
//	fall below the threshold for which it is acceptable to select sort the array
//
// parameters:
//	- in_chan:	channel of numbers to be sorted
//	- out_chan:	channel of sorted numbers
//
// no explicit return
func selection_sort( in_chan chan []int, out_chan chan int ) {
	numbers := <- in_chan;
	for i := 0; i < len( numbers ); i++ {
		min := 0;

		for j := 0; j < len( numbers ); j++ {
			if ( numbers[min] == -1 || (numbers[j] != -1 && numbers[j] < numbers[min] )) {
				min = j;
			}
		}
		out_chan <- numbers[min];
		numbers[min] = -1;
    }
}
